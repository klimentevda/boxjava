package pages;


import pages.mail.LoginPage;

import static selenium.SeleniumFactory.init;


public class Pages {

    private static ThreadLocal<Pages> instance = new ThreadLocal<>();

    public static Pages get(){
        if (instance.get() == null){
            instance.set(new Pages());
        }
        return instance.get();
    }

    public static Pages initialize(){
        instance.set(new Pages());
        return instance.get();
    }


    public final LoginPage loginPage;


    private Pages(){
           loginPage = init(LoginPage.class);
    }
}
