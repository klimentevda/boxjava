package utils.assertions;

import org.testng.Assert;

/**
 * Provide a way to do multiple assertion without losing information about exceptions
 */
public class CustomAssert{

    /**
     * Take screen shot on every assertion failure
     */
    public static void enableAutoScreenshots(){
        AssertionBlock.getInstance().setTakeScreenOnEveryFailure(true);
    }

    /**
     * Disable taking screen shot on every assertion failure
     */
    public static void disableAutoScreenshots(){
        AssertionBlock.getInstance().setTakeScreenOnEveryFailure(false);
    }

    /**
     * Start soft assertion block
     */
    public static void start(){
        AssertionBlock.getInstance().start();
    }

    /**
     * Stop soft assertion block
     */
    public static void stop(){
        AssertionBlock.getInstance().stop();
    }

    public static void assertAll(){
        stop();
        start();
    }

    public static void assertTrue(boolean b, String message){
        try {
            Assert.assertTrue(b, processMessage(message));
        }catch (AssertionError error){
            AssertionBlock.getInstance()
                    .processException(error);
        }

    }

    public static void assertFalse(boolean b, String message){
        try {
            Assert.assertFalse(b, processMessage(message));
        }catch (AssertionError error){
            AssertionBlock.getInstance()
                    .processException(error);
        }
    }

    public static void assertEquals(Object actual, Object expected, String message){
        try {
            Assert.assertEquals(actual, expected, processMessage(message));
        }catch (AssertionError error){
            AssertionBlock.getInstance()
                    .processException(error);
        }
    }

    public static void assertNotEquals(Object actual, Object expected, String message) {
        try {
            Assert.assertNotEquals(actual, expected, processMessage(message));
        }catch (AssertionError error){
            AssertionBlock.getInstance()
                    .processException(error);
        }
    }

    public static void assertNull(Object obj, String message){
        try {
            Assert.assertNull(obj, processMessage(message));
        }catch (AssertionError error){
            AssertionBlock.getInstance()
                    .processException(error);
        }
    }

    public static void assertNotNull(Object obj, String message){
        try {
            Assert.assertNotEquals(obj , null, processMessage(message));
        }catch (AssertionError error){
            AssertionBlock.getInstance()
                    .processException(error);
        }
    }

    private static String processMessage(String message){
        String result = message;
        if ((!result.endsWith(":") ||
                (!result.endsWith(": "))||
                (!result.endsWith("."))
            ))
        {
            result += ":";
        }
        return result;
    }
}
