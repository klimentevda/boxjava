package utils.assertions;

import core.selenium.driver.DriverManager;
import core.selenium.driver.DriverUtils;
import core.testng.steps.Steps;
import core.utility.TestReporter;
import io.qameta.allure.Allure;

import java.util.ArrayList;
import java.util.List;

/** Process AssertionErrors and collect it on start and throw all in end**/
public class AssertionBlock {
    /** Multi-ton instance **/
    private static ThreadLocal<AssertionBlock> driverThread = new ThreadLocal<AssertionBlock>();

    private boolean takeScreenOnEveryFailure = true;

    /** Return instance of Assertion block**/
    public static AssertionBlock getInstance() {
        if (driverThread.get() == null) {
            synchronized (AssertionBlock.class) {
                driverThread.set(new AssertionBlock());
            }
        }
        return driverThread.get();
    }

    /** List of exception collected during soft run**/
    private List<Throwable> errors;
    /** Soft mode marker **/
    private boolean softMode = false;

    /** Private constructor**/
    private AssertionBlock(){
        initialize();
    }

    /** Initialization block**/
    protected void initialize(){
        errors = new ArrayList<>();
    }

    /** Getter for stored errors**/
    public List<Throwable> getErrors() {
        return errors;
    }

    /** Start collection assertion failures that processed with <see>processException</see> function**/
    public void start(){
        if (!softMode) {
            initialize();
        }else{
            //TODO THINK where should be class responsible for work with stack trace
            String trace = "";
            for (StackTraceElement element : Thread.currentThread().getStackTrace()){
                trace +="\t" +  element.toString() + "\n";
            }
            stop();
            initialize();
            TestReporter.log("Reused 'start' method of Custom Assertion.\n Trace:\n" + trace);
        }
        softMode = true;
    }

    /** Stop collecting assertion failures and throw exception if errors present**/
    public void stop(){
        softMode = false;
        List<Throwable> errors = getErrors();
        //If errors present form big and informative exception and throw it
        if (errors.size() > 0){

            String message = "";
            //If one error - skip showing errors
            if (errors.size() == 1){
                message += errors.get(0).getMessage();
            }else
                //If more than one error - show numbers of exception
                {
                for (int i = 0; i < errors.size(); i++) {
                    //formatting output
                    if (i != 0)
                        message +="\t";

                    //add exception to log
                    message += Integer.toString(i + 1) + ". " + errors.get(i).getMessage();

                    //formatting output
                    if (i != errors.size()-1)
                        message += "\n";
                }
            }

            //form exception and throw
            AssertionError bigException = new AssertionError(message);
            bigException.setStackTrace(errors.get(0).getStackTrace());
            throw bigException;
        }
    }

    public void setTakeScreenOnEveryFailure(boolean takeScreenOnEveryFailure) {
        this.takeScreenOnEveryFailure = takeScreenOnEveryFailure;
    }

    /** Process exception**/
    public void processException(AssertionError error){
        if (softMode){
            errors.add(error);
            makeAttachments(error);
        }else{
            throw error;
        }
    }

    public void makeAttachments(AssertionError error){
        if ((DriverManager.isRunned()
                && takeScreenOnEveryFailure)) {
            Allure.getLifecycle().addAttachment(
                    "STEP-" + Steps.getInstance().getLastStep().getName() + "-Failure",
                    "image/png",
                    "",
                    new DriverUtils().takeScreenShot());
        }

    }

}
