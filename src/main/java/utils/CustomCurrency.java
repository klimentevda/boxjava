package utils;

/**
 * Class to work with currency values
 * like following '5000.00 ALL'
 */
public class CustomCurrency {

    private final String amount;
    private final String currency;
    private final String splitter;

    public static CustomCurrency byValue(int amount){
        return new Builder().amount(amount).build();
    }

    public static CustomCurrency byText(String fullText){
        return new Builder().buildFromValue(fullText);
    }

    private CustomCurrency(String amount, String currency, String splitter){
        this.amount = amount;
        this.currency = currency;
        this.splitter = splitter;
    }

    /**
     * @return amount of payment as string
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @return amount of payment as integer
     */
    public int getValue(){
        return Integer.valueOf(amount);
    }

    /**
     * @return currency 'ALL'
     */
    public String getCurrency() {
        return currency;
    }

    public CustomCurrency add(int amount){
        return new Builder()
                .amount(getValue() + amount)
                .currency(currency)
                .splitter(splitter)
                .build();
    }

    /**
     * Return % from current value
     * @param percents
     * @return
     */
    public CustomCurrency getPercents(int percents){
        return new Builder()
                .amount(getValue() * percents / 100)
                .currency(currency)
                .splitter(splitter)
                .build();
    }

    /**
     * @return text in format '5000.00 ALL'
     */
    public String getText(){
        return amount + splitter + currency;
    }

    /**
     * @return text in format '5000.00'
     */
    public String getTextWtihoutCurrency(){
        return amount + splitter.trim();
    }

    @Override
    public String toString() {
        return getText();
    }

    public static class Builder{
        private String amount = "";
        private String currency = "ALL";
        private String splitter = ".00 ";

        public Builder amount(String amount){
            this.amount = amount;
            return this;
        }

        public Builder amount(int amount){
            this.amount = Integer.toString(amount);
            return this;
        }

        public Builder currency(String currency){
            this.currency=currency;
            return this;
        }

        public Builder splitter(String splitter){
            this.splitter = splitter;
            return this;
        }

        public CustomCurrency buildFromValue(String text){
            amount(text.split(splitter)[0]);
            currency(text.split(splitter)[1]);
            return build();
        }

        public CustomCurrency build(){
            if (amount.isEmpty()) throw new RuntimeException("Empty value");
            return new CustomCurrency(amount, currency, splitter);
        }
    }

}
