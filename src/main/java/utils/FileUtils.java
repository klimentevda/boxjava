package utils;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

public class FileUtils {

    public static String getPathFromResources(String path){
        try {
            URL test = FileUtils.class.getClassLoader().getResource(path);
            URI uri = new URL(test.toString().replaceAll(" ", "%20")).toURI();
            return  Paths.get(uri).toAbsolutePath().toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
