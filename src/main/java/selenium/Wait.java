package selenium;

import core.selenium.driver.DriverUtils;
import core.testng.steps.Steps;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Wait {

    public static void sleep(int seconds){
        try {
            Steps.subInfo("Sleep for " + seconds + " seconds.", "");
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static WebElement forVisible(WebElement element){
        return DriverUtils.getElementWait()
                .withMessage("Wait for [" + element + "] to be visible")
                .until(ExpectedConditions.visibilityOf(element));
    }

    public static WebElement forVisible(By locator){
        return DriverUtils.getElementWait()
                .withMessage("Wait for " + locator + " to be visible")
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static WebElement forClickable(WebElement element){
        return DriverUtils.getElementWait()
                .withMessage("Wait for [" + element + "] to be clickable")
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static WebElement forClickable(By locator){
        return DriverUtils.getElementWait()
                .withMessage("Wait for [" + locator + "] to be clickable")
                .until(ExpectedConditions.elementToBeClickable(locator));
    }

    public static void forInvisible(WebElement element){
        DriverUtils.getElementWait()
                .withMessage("Wait for [" + element + "] to be invisible")
                .until(ExpectedConditions.invisibilityOf(element));
    }
}
