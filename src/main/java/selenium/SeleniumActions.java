package selenium;

import org.openqa.selenium.WebElement;

public class SeleniumActions {

    private final WebElement element;

    public SeleniumActions(WebElement element){
        this.element = element;
    }

    public SeleniumActions sendKeys(CharSequence charSequence){
        SeleniumMethods.sendKeys(element, charSequence);
        return this;
    }

    public SeleniumActions click(){
        SeleniumMethods.click(element);
        return this;
    }

    public SeleniumActions clear(){
        SeleniumMethods.clear(element);
        return this;
    }


}
