package selenium;

import core.selenium.driver.DriverManager;
import core.selenium.driver.DriverUtils;
import core.testng.steps.Steps;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.FileUtils;

public class SeleniumMethods {

    public static void typeAndEnter(WebElement element, String text){
        type(element, text);
        sendKeys(element,Keys.RETURN);
    }

    public static void type(WebElement element, String text){
        click(element);
        clear(element);
        sendKeys(element, text);
    }

    public static void mouseOver(WebElement element){
        Actions actions = getActions();
        actions
                .moveToElement(element)
                .perform();
    }

    public static void click(WebElement element){
        try{
            element.click();
        }catch (WebDriverException ex){
            scrollToTop();
            Wait
                    .forClickable(element)
                    .click();
        }
    }

    public static void sendKeys(WebElement element, CharSequence sequence){
        try{
            element.sendKeys(sequence);
        }catch (WebDriverException ex){
            scrollToTop();
            Wait
                    .forClickable(element)
                    .sendKeys(sequence);
        }
    }

    public static void clear(WebElement element){
        try{
            element.clear();
        }catch (WebDriverException ex){
            scrollToTop();
            Wait
                    .forClickable(element)
                    .clear();
        }
    }

    public static void clickOnCrazyAjax(WebElement element){
        int crazinessAmount = 3;
        for (int i=0; i<crazinessAmount; i++)
            click(element);
    }

    public static void upload(WebElement element, String path){
        String fullPath = FileUtils.getPathFromResources(path);
        Steps.subInfo("Uploading of file '" + path + "'", "Full path: " + fullPath);
        sendKeys(element, fullPath);
    }

    public static void scrollToTop(){
        DriverUtils.javascriptExecutor().executeScript("if ((window.scrollY!=0) || (window.scrollX!=0)) window.scrollTo(0, 0);");
    }

    public static boolean isElementExist(WebElement element){
        try {
            return element.isDisplayed();
        }catch (NoSuchElementException e){
            return false;
        }
    }

    public static Actions getActions(){
        return new Actions(DriverManager.currentDriver());
    }
}
