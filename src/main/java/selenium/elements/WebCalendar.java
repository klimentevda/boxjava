package selenium.elements;

import selenium.SeleniumFactory;
import selenium.SeleniumMethods;
import utils.CustomDate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WebCalendar {

    By prevoiousMonth = By.xpath(".//span[@title='Previous Month']");

    By nextMonth = By.xpath(".//span[@title='Next Month']");

    By selectedMonth = By.xpath(".//th[@title='Select Month']");

    //td[@data-day='11/25/2018']
    private final String DAY = ".//td[@data-day='%s']";

    private WebElement self;

    public WebCalendar(WebElement element){
        self = element;
    }

    public void selectData(CustomDate date){
        SeleniumMethods.click(self);
        String strDate = date.getDate("MM/dd/yyyy");

        WebElement button;
        if (date.getTimestamp() > new CustomDate().getTimestamp()) {
            button = self.findElement(nextMonth);
        }else{
            button = self.findElement(prevoiousMonth);
        }

        int retry = 25;
        while ((SeleniumFactory.findElement(self, DAY, strDate) == null) && (retry != 0)){
            retry--;
            button.click();
        }

        if (retry != 0)
            SeleniumFactory.findElement(self, DAY, strDate).click();

    }
}
