package selenium;

import core.selenium.driver.DriverManager;

import java.util.ArrayList;
import java.util.List;

//TODO move to core functionality after testing and refactoring
public class DriverUtilsStaging {

    public static void switchToLastWindow(){
        List<String> windowHandles = new ArrayList<>(DriverManager.currentDriver().getWindowHandles());

        DriverManager
                .currentDriver()
                .switchTo()
                .window(
                        windowHandles.get(windowHandles.size()-1)
                );
    }

    public static void closeAndNavigateToPreviousWindow(){
        DriverManager.currentDriver().close();
        switchToLastWindow();
    }

    public static void refresh(){
        DriverManager.currentDriver().navigate().refresh();
    }

    public static void switchToMainWindow(){
        List<String> windowHandles = new ArrayList<>(DriverManager.currentDriver().getWindowHandles());

        DriverManager
                .currentDriver()
                .switchTo()
                .window(
                        windowHandles.get(0)
                );
    }
}
