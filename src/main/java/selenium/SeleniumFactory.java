package selenium;

import core.selenium.driver.DriverManager;
import core.selenium.driver.DriverUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SeleniumFactory {

    public static <T> T init(Class<T> type){
        return PageFactory.initElements(DriverManager.currentDriver(), type);
    }

    public static WebElement getElement(By locator){
        return DriverUtils.getElementWait()
                .withMessage("Location of element " + locator)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static WebElement getElement(String pattern, Object... args){
        By locator = By.xpath(String.format(pattern, args));
        return DriverUtils.getElementWait()
                .withMessage("Location of element " + locator)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static WebElement findElement(String pattern, Object... args){
        By locator = By.xpath(String.format(pattern, args));
        try {
            return DriverManager.currentDriver().findElement(locator);
        }catch (NoSuchElementException ex){
            return null;
        }
    }

    public static WebElement findElement(WebElement element, String pattern, Object... args){
        By locator = By.xpath(String.format(pattern, args));
        try {
            return element.findElement(locator);
        }catch (NoSuchElementException ex){
            return null;
        }
    }

}
