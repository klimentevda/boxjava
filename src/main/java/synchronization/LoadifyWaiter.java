package synchronization;



import core.selenium.conditions.CustomConditions;
import core.selenium.driver.DriverManager;
import core.selenium.driver.DriverUtils;
import core.selenium.driver.WebDriverSettings;
import core.selenium.driver.listeners.EventWaiter;
import core.selenium.driver.listeners.IWaiter;

import java.time.Duration;

@EventWaiter(priority = 115)
/**
 * Класс ожидающий document.ReadyState
 */
public class LoadifyWaiter implements IWaiter {

    @Override
    public void synchronize() {
        String url = DriverManager.currentDriver().getCurrentUrl();
        if (url.contains("/applications/todo/")) {
            DriverUtils
                    .getWait(WebDriverSettings.getInstance().getAjaxTimeout())
                    .withMessage("Waiting for classes to not have 'has_loadify2'")
                    .pollingEvery(Duration.ofMillis(100))
                    .until(new CustomConditions().notExistByID("has_loadify2"));
            DriverUtils
                    .getWait(WebDriverSettings.getInstance().getAjaxTimeout())
                    .pollingEvery(Duration.ofMillis(100))
                    .withMessage("Waiting for classes to not have 'has_loadify'")
                    .until(new CustomConditions().notExistByID("has_loadify"));
        }
    }
}
