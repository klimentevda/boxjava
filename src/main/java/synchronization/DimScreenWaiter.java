package synchronization;

import core.selenium.conditions.CustomConditions;
import core.selenium.driver.DriverUtils;
import core.selenium.driver.WebDriverSettings;
import core.selenium.driver.listeners.EventWaiter;
import core.selenium.driver.listeners.IWaiter;

@EventWaiter(priority = 103)
/**
 * Класс ожидающий document.ReadyState
 */
public class DimScreenWaiter implements IWaiter {

    @Override
    public void synchronize() {
        long millis = System.currentTimeMillis();
        //String url = DriverManager.currentDriver().getCurrentUrl();
        //if (url.contains("/applications/todo/"))
        DriverUtils
                .getWait(WebDriverSettings.getInstance().getAjaxTimeout())
                .withMessage("Waiting for invisibility of 'dimScreen'")
                .until(new CustomConditions().notExistByID("dimScreen"));
        //if ((Boolean)(((JavascriptExecutor) DriverManager.currentDriver()).executeScript("return jQuery.active == 1"))) {
        //    System.out.println("!Warning jquery after dimScreen");
            DriverUtils
                    .getWait(WebDriverSettings.getInstance().getAjaxTimeout())
                    .withMessage("Waiting for jQuery to not be active")
                    .until(new CustomConditions().jQueryLoad);
       // }
    }
}
