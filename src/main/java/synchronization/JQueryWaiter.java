package synchronization;

import core.selenium.conditions.CustomConditions;
import core.selenium.driver.DriverUtils;
import core.selenium.driver.WebDriverSettings;
import core.selenium.driver.listeners.EventWaiter;
import core.selenium.driver.listeners.IWaiter;


@EventWaiter(priority = 101)
/**
 * Класс ожидающий document.ReadyState
 */
public class JQueryWaiter implements IWaiter {

    @Override
    public void synchronize() {
        DriverUtils
                .getWait(WebDriverSettings.getInstance().getAjaxTimeout())
                .withMessage("Waiting for jQuery to not be active")
                .until(new CustomConditions().jQueryLoad);
    }
}
