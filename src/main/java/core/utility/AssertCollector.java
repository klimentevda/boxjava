package core.utility;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static core.utility.Constants.NEW_LINE_REGEX;

public class AssertCollector {
    public static ITestResult result;

    private static final Map<ITestResult, List<Throwable>> verificationFailuresMap = new HashMap<>();

    public static void assertTrue(boolean condition) {
        Assert.assertTrue(condition);
    }

    public static void assertTrue(boolean condition, String message) {
        try {
            Assert.assertTrue(condition, message);
            TestReporter.success(message
                    + TestReporter.getActualAndExpectedMessage(String.valueOf(condition), Boolean.TRUE.toString()));
        } catch (AssertionError e) {
            TestReporter.fail(message
                    + TestReporter.getActualAndExpectedMessage(String.valueOf(condition), Boolean.TRUE.toString()));
            Assert.fail(message);
        }
    }

    public static void assertFalse(boolean condition) {
        Assert.assertFalse(condition);
    }

    public static void assertFalse(boolean condition, String message) {
        try {
            Assert.assertFalse(condition, message);
            TestReporter.success(message
                    + TestReporter.getActualAndExpectedMessage(String.valueOf(condition), Boolean.FALSE.toString()));
        } catch (AssertionError e) {
            TestReporter.fail(message
                    + TestReporter.getActualAndExpectedMessage(String.valueOf(condition), Boolean.FALSE.toString()));
            Assert.fail(message);
        }
    }

    public static void assertEquals(String actual, String expected) {
        assertEquals(actual, expected, "");
    }

    public static void assertEquals(Object actual, Object expected, String message) {
        try {
            Assert.assertEquals(actual, expected, message);
            TestReporter.success(message + TestReporter.getActualAndExpectedMessage(actual.toString(), expected.toString()));
        } catch (Throwable e) {
            TestReporter.fail(message + TestReporter.getActualAndExpectedMessage(actual.toString(), expected.toString()));
            Assert.fail(message);
        }
    }

    public static void assertEquals(Collection<?> actual, Collection<?> expected, String message) {
        try {
            Assert.assertEquals(actual, expected, message);
            TestReporter.success(message + TestReporter.getActualAndExpectedMessage(actual, expected));
        } catch (Throwable e) {
            TestReporter.fail(message + TestReporter.getActualAndExpectedMessage(actual, expected));
            Assert.fail(message);
        }
    }

    public static void assertEquals(ITestResult testResult, String actual, String expected, String message) {
        try {
            Assert.assertEquals(actual, expected, message);
        } catch (Throwable e) {
            TestReporter.fail(testResult.getTestName() + " test FAIL " + testResult.getThrowable());
            Assert.fail(message);
        }
    }

    /**
     * This method uses library assertJ for once-only comparison. "Try-catch" is used for remaining old logging logic,
     * but there is RuntimeException in the end that test can fail in appropriate situation.
     *
     * @param actual   - actual result obtained from UI
     * @param expected - expected result obtained from data file, db, etc
     * @param message  -  message send to reporter
     */
    public static void assertEqualsJ(Object actual, Object expected, String message) {
        try {
            assertThat(actual).isEqualTo(expected);
            TestReporter.success(message + TestReporter.getActualAndExpectedMessage(actual.toString(), expected.toString()));
        } catch (AssertionError e) {
            TestReporter.fail(message + TestReporter.getActualAndExpectedMessage(actual.toString(), expected.toString()));
            Assert.fail(message);
        }
    }

    public static void assertEqualsJTextStartsWith(String actualText, String firstCharacters, String message) {
        try {
            assertThat(actualText).startsWith(firstCharacters);
            TestReporter.success(message + TestReporter.getActualAndExpectedMessage(actualText, "Text starts with " + firstCharacters));
        } catch (AssertionError e) {
            TestReporter.fail(message + TestReporter.getActualAndExpectedMessage(actualText, "Text starts with " + firstCharacters));
            Assert.fail(message);
        }
    }

    public static void checkListWebElementsHaveExpectedText(String expectedText, List<WebElement> list) {
        for (WebElement webElement : list) {
            AssertCollector.assertEqualsJ(webElement.getText().replaceAll(NEW_LINE_REGEX, " "), expectedText, "COMPARISON OF WEB-ELEMENT TEXT");
        }
    }

    public static void checkListWebElementsHaveExpectedTextIgnoreCase(String expectedText, List<WebElement> list) {
        for (WebElement webElement : list) {
            assertThat(webElement.getText()).as("COMPARISON OF WEB-ELEMENT TEXT").isEqualToIgnoringCase(expectedText);
        }
    }

    public static void checkWebElementHaveExpectedTextIgnoreCase(String expectedText, WebElement element) {
        assertThat(element.getText().trim()).as("COMPARISON OF WEB-ELEMENT TEXT").isEqualToIgnoringCase(expectedText);
    }

    public static void checkWebElementHaveExpectedText(String expectedText, WebElement element) {
        assertThat(element.getText()).as("COMPARISON OF WEB-ELEMENT TEXT").isEqualTo(expectedText);
    }

    public static void checkListWebElementsContainsExpectedText(String expectedText, List<WebElement> list) {
        for (WebElement webElement : list) {
            assertThat(webElement.getText()).as("COMPARISON OF WEB-ELEMENT PARTIAL TEXT").contains(expectedText);
        }
    }

    public static void checkListWebElementsContainsExpectedTextIgnoreCase(String expectedText, List<WebElement> list) {
        for (WebElement webElement : list) {
            assertThat(webElement.getText()).as("COMPARISON OF WEB-ELEMENT PARTIAL TEXT").containsIgnoringCase(expectedText);
        }
    }

    public static void assertThatListContainSubList(List<String> listToVerify, List<String> options) {
        for (String option : options) {
            try {
                assertThat(listToVerify).contains(option);
                TestReporter.success(String.format("assertThat - Actual list contain Condition '%s'", option));
            } catch (Throwable e) {
                TestReporter.fail(String.format("assertThat - Actual list not contain Condition '%s'", option));
                throw new RuntimeException(String.format("Verification is not passed - ACTUAL LIST not contain " +
                        "EXPECTED VALUES %s", option));
            }
        }
    }

    public static void assertThatListContainOneExpectedOption(List<String> list, String expectedOption) {
        try {
            assertThat(list).contains(expectedOption);
            TestReporter.success("assertThat - " + String.format("Actual list contain Condition '%s'", expectedOption));
        } catch (Throwable e) {
            TestReporter.fail("assertThat - " + String.format("Actual list not contain Condition '%s'", expectedOption));
            throw new RuntimeException("Verification is not passed - ACTUAL LIST not contain EXPECTED VALUES " + expectedOption);
        }
    }

    public static void assertThatTextContainsExpectedText(String actualText, String expectedText, String message) {
        try {
            assertThat(actualText).contains(expectedText);
            TestReporter.success(message + String.format(" - Actual Text '%s' contain '%s' ", actualText, expectedText));
        } catch (AssertionError e) {
            TestReporter.fail(message + String.format(" - Actual Text '%s' not contain '%s' ", actualText, expectedText));
            Assert.fail(message);
        }
    }

    public static void assertThatTextContainsExpectedTextIgnoreCase(String actualText, String expectedText, String message) {
        try {
            assertThat(actualText).containsIgnoringCase(expectedText);
            TestReporter.success(message + String.format(" - Actual Text '%s' contain '%s' ", actualText, expectedText));
        } catch (AssertionError e) {
            TestReporter.fail(message + String.format(" - Actual Text '%s' not contain '%s' ", actualText, expectedText));
            Assert.fail(message);
        }
    }

    public static void fail(String message) {
        Assert.fail(message);
    }

    private static List<Throwable> getVerificationFailures() {
        List<Throwable> verificationFailures = verificationFailuresMap.get(Reporter.getCurrentTestResult());
        return verificationFailures == null ? new ArrayList<>() : verificationFailures;
    }

    private static void addVerificationFailure(Throwable e) {
        List<Throwable> verificationFailures = getVerificationFailures();
        verificationFailuresMap.put(Reporter.getCurrentTestResult(), verificationFailures);
        verificationFailures.add(e);
    }
}
