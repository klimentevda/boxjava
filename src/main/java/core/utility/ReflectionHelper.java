package core.utility;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.annotation.Annotation;
import java.util.Set;

public class ReflectionHelper {


    public static Set<Class<?>> getInstancesOf(Class<?> type) {
        Reflections reflections = new Reflections(type.getPackage().getName());
        return (Set<Class<?>>) reflections.getSubTypesOf(type);
    }

    public static Set<Class<?>> getAnnotatedWith(Class<? extends Annotation> type) {
        Reflections reflections = new Reflections(
                new ConfigurationBuilder()
                        .setUrls(ClasspathHelper.forJavaClassPath())
                        .addUrls(ClasspathHelper.forManifest())
        );
        Set<Class<?>> types = reflections.getTypesAnnotatedWith(type);
        return types;
    }
}
