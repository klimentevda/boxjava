package core.utility;


import core.logger.MangoFinanceLogger;

import java.sql.*;
import java.util.*;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;


public class DbManager implements AutoCloseable {
    private static final Logger LOGGER = MangoFinanceLogger.getMangoLogger().getLogger();
    private Connection conn;

    //========================UPDATE QUERY================================================
    public static final String UPDATE_APPLICATION_SOURCE_QUERY = "UPDATE applications SET source = ? WHERE id = ?";
    public static final String UPDATE_SYSTEM_SETTING = "UPDATE mod_system_settings set value  = ? " +
            "where \"key\" = ? and namespace=?";
    public static final String UPDATE_APPLICATION_IS_TEST_QUERY = "UPDATE applications SET is_test = ? WHERE id = ?";
    //========================INSERT QUERY================================================
    private static final String INSERT_AUTH_TYPE_QUERY = "INSERT INTO applications_authorisations (application_id, " +
            "client_id, created_by, updated_by, authentication_type, country, createdat, updatedat) VALUES " +
            "( ?, ?, 1, 1, ?, 'gl', now(), now())";
    //========================DELETE QUERY================================================
    private static final String DELETE_APPLICATION_AUTH_QUERY = "DELETE FROM applications_authorisations WHERE application_id = ?";

    private Connection setConnectionToPostgreSqlDb() {
        Properties props = PropertiesLoader.load();
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(props.getProperty("db.url"),
                    props.getProperty("db.user"), props.getProperty("db.user.password"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, "Unable to find Postgre SQL JDBC Driver.Include in your library path! " + e.getMessage());
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, "Connection Failed! Check output console " + e.getMessage());
            return null;
        }
        if (conn == null) {
            LOGGER.log(Level.WARNING, "Failed to make connection!");
        } else {
            LOGGER.log(Level.INFO, "Connected to DB");
        }
        return conn;
    }

    /**
     * This method executes sql request to update application is_test, as an input parameters
     * need to specify is_test type that should be setted up and application id
     *
     * @param isTest - can be true or false
     * @param applicationId - id of application in which you want to update isTest
     */
    public void updateApplicationIsTest(boolean isTest, int applicationId) {
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = ((Supplier<PreparedStatement>) () -> {
            try {
                PreparedStatement s = conn.prepareStatement(
                        UPDATE_APPLICATION_IS_TEST_QUERY);
                s.setBoolean(1, isTest);
                s.setInt(2, applicationId);
                return s;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }).get()
        ) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during update Application is_test operation " + e.getMessage());
        }
    }

    /**
     * This method executes sql request to update application source, as an input parameters
     * need to specify source type that should be setted up and application id
     *
     * @param desiredSource - can be branch or web app
     * @param applicationId - id of application in which you want to update source
     */
    public void updateApplicationSource(String desiredSource, int applicationId) {
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = ((Supplier<PreparedStatement>) () -> {
            try {
                PreparedStatement s = conn.prepareStatement(
                        UPDATE_APPLICATION_SOURCE_QUERY);
                s.setString(1, desiredSource);
                s.setInt(2, applicationId);
                return s;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }).get()
        ) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during update Application Source operation " + e.getMessage());
        }
    }

    /**
     * This method executes sql request to update System setting.
     * Need to specify setting value:
     * specify setting value = '1', services will be Enable
     * specify setting value = '0', services will be Disable
     * Specify setting key = which of settings want to update
     * Specify setting namespace = which of the module(general,spl,installment) want to update
     *
     * @param parameters - setting value,key and namespace;
     */
    public void updateSystemSetting(Object... parameters) {
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = getPreparedStatement(UPDATE_SYSTEM_SETTING, parameters)) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during update System setting " + e.getMessage());
        }
    }

    public void setAppAuthTypeToHyphen(int applicationId) {
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = ((Supplier<PreparedStatement>) () -> {
            try {
                PreparedStatement s = conn.prepareStatement(
                        DELETE_APPLICATION_AUTH_QUERY);
                s.setInt(1, applicationId);
                return s;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }).get()
        ) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during Delete Application Authentication type " + e.getMessage());
        }
    }

    public void insertApplicationAuthType(int applicationId, int clientId, String desiredAuthType) {
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = ((Supplier<PreparedStatement>) () -> {
            try {
                PreparedStatement s = conn.prepareStatement(
                        INSERT_AUTH_TYPE_QUERY);
                s.setInt(1, applicationId);
                s.setInt(2, clientId);
                s.setString(3, desiredAuthType);
                return s;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }).get()
        ) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during Delete Application Authentication type " + e.getMessage());
        }
    }

    /**
     * This method selects data from the database on the transmitted request
     *
     * @param query      - data sampling request
     * @param parameters - parameters for the query (must be passed in the same order as in the query)
     * @return - data in the format Map<String, String>
     */
    public Map<String, String> selectByQueryWithReturnMapStringString(String query, Object... parameters) {
        Map<String, String> dataMap = new HashMap<>();
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = getPreparedStatement(query, parameters)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                IntStream.range(1, rs.getMetaData().getColumnCount() + 1).boxed().forEach(value -> {
                    try {
                        dataMap.put(rs.getMetaData().getColumnName(value), rs.getString(value));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
            }
            return dataMap;
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during SELECT query operation " + e.getMessage());
        }
        return null;
    }

    public Map<String, Integer> selectByQueryWithReturnMapStringInteger(String query, Object... parameters) {
        Map<String, Integer> dataMap = new HashMap<>();
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = getPreparedStatement(query, parameters)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                IntStream.range(1, rs.getMetaData().getColumnCount() + 1).boxed().forEach(value -> {
                    try {
                        dataMap.put(rs.getMetaData().getColumnName(value), rs.getInt(value));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
            }
            return dataMap;
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during SELECT query operation " + e.getMessage());
        }
        return null;
    }

    public List<String> selectByQueryWithReturnListString(String query, Object... parameters) {
        List<String> dataMap = new ArrayList<>();
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = getPreparedStatement(query, parameters)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                dataMap.add(rs.getString(1));
            }
            return dataMap;
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during SELECT query operation " + e.getMessage());
        }
        return null;
    }

    public String selectByQueryWithReturnString(String query, Object... parameters) {
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = getPreparedStatement(query, parameters)) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during SELECT query operation " + e.getMessage());
        }
        return null;
    }

    public Integer selectByQueryWithReturnInteger(String query, Object... parameters) {
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = getPreparedStatement(query, parameters)) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during SELECT query operation " + e.getMessage());
        }
        return null;
    }

    public Boolean selectByQueryWithReturnBoolean(String query, Object... parameters) {
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = getPreparedStatement(query, parameters)) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during SELECT query operation " + e.getMessage());
        }
        return null;
    }

    private PreparedStatement getPreparedStatement(String query, Object... parameters) {
        int indexCounter = 1;
        try {
            PreparedStatement s = conn.prepareStatement(query);
            for (Object param : parameters) {
                if (param instanceof String) {
                    s.setString(indexCounter, String.valueOf(param));
                } else if (param instanceof Integer) {
                    s.setInt(indexCounter, (Integer) param);
                } else if (param instanceof Boolean) {
                    s.setBoolean(indexCounter, (Boolean) param);
                } else {
                    throw new RuntimeException("The type of the incoming parameter is not defined");
                }
                indexCounter++;
            }
            return s;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Use this method, when you have a full update request that needs to be performed
     *
     * @param query - string that contains full SQL query with parameters
     */
    public void executeUpdateQuery(String query) {
        setConnectionToPostgreSqlDb();
        try (PreparedStatement stmt = ((Supplier<PreparedStatement>) () -> {
            try {
                return conn.prepareStatement(query);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }).get()
        ) {
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            TestReporter.fail("EXCEPTION during update operation " + e.getMessage());
        }
    }

    @Override
    public void close() {
        try {
            this.conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, "Connection CLOSE operation Failed! Check output console " + e.getMessage());
        }
    }
}
