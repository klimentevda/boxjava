package core.utility;

import io.qameta.allure.Allure;
import core.logger.MangoFinanceLogger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import core.selenium.driver.DriverManager;
import core.testng.steps.Steps;

import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;

import static java.lang.String.format;


/**
 * @author Alisa Demennikova
 */
public class TestReporter {


    public static void testTitle(String title) {
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.FINE, "Test title: " + title);
        Steps.info("Test title: " + title);
    }

    /**
     * Log on info level
     *
     * @param message
     */
    public static void logInfo(String message) {
        MangoFinanceLogger.getMangoLogger().getLogger().info(message);
    }

    /**
     * This method will mark your message as a step in current executed test in report
     *
     * @param step - message that corresponds to a step in executed scenario
     */
    public static void step(String step) {
        Steps.info(step);
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.FINE, step);
    }

    /**
     * This method mark your message as a part of step log,
     * so, if your message is not a step, but an important and should be included to report
     * you can use this method to add an intermediate verifications between steps
     *
     * @param stepInfo - message that should be logged to report of current test that executed
     */
    public static void stepInfo(String stepInfo) {
        Steps.info("Step info: " + stepInfo);
        Steps.description("Step info: " + stepInfo);
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.FINE, "Step info: " + stepInfo);
    }

    /**
     * Let to add to report comparison results in format of:
     * Expected: [expected]
     * Actual: [actual]
     *
     * @param actual   - actual comparison value
     * @param expected - expected comparison value
     */
    public static void reportActualAndExpected(String actual, String expected) {
        String message = getActualAndExpectedMessage(actual, expected);
        stepInfo(message);
    }

    /**
     * Let to add log information to 'Log Output' report section
     *
     * @param simpleLogMessage - message that should be added to 'Log Output' section
     */
    public static void log(String simpleLogMessage) {
        //Steps.info("Log: " + simpleLogMessage);
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.FINE, "Log: " + simpleLogMessage);
    }

    /**
     * Mark your logged message in red color that indicates the Fail
     *
     * @param failureMessage - message that should be mark as fail
     */
    public static void fail(String failureMessage) {
        Steps.info("fail: " + failureMessage);
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.WARNING, "fail: " + failureMessage);
    }

    /**
     * Mark your logged message in green color that indicates the Success
     *
     * @param successMessage - message that should be mark as success
     */
    public static void success(String successMessage) {
        Steps.info("success: " + successMessage);
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.FINE, "success: " + successMessage);
    }

    /**
     * Mark your logged message in yellow color that indicates the Skip
     *
     * @param skipMessage - message that should be mark as skip
     */
    public static void skip(String skipMessage) {
        Steps.info("skip: " + skipMessage);
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.WARNING, "skip: " + skipMessage);
    }

    /**
     * Let you to add screenshot any time you want in any place in the Framework
     *
     * @param screenshotName - simple name of future added screenshot
     */
    //@Attachment(value = "Screen INFO: {screenshotName}", type = "image/png")
    public static byte[] addScreenshotToReport(String screenshotName) {
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.FINE, "Take screenshot for information: " + screenshotName);
        byte[] screen = ((TakesScreenshot) DriverManager.currentDriver()).getScreenshotAs(OutputType.BYTES);
        Allure.getLifecycle().addAttachment("Screenshot '" + screenshotName + "'", "image/png", "", screen);
        return screen;
    }

    /**
     * Appends border with message in report in format:
     * //================= YOUR_MESSAGE_HERE =================//
     *
     * @param borderMessage - message of the border
     */
    public static void appendBorderInReport(String borderMessage) {
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.FINE, format("================= %s =================", borderMessage));
        //Steps.info(format("================= %s =================", borderMessage));
    }

    /**
     * Let to get a simple string in html format with comparison resi=ult of actual and expected
     *
     * @param actual   - actual comparison value
     * @param expected - expected comparison value
     * @return - a string in simple html format, that could be then pasted in any other place or log message
     */
    public static String getActualAndExpectedMessage(Object actual, Object expected) {
        return "\nExpected: [" + expected + "] \nActual: [" + actual + "]";
    }

    /**
     * Should be pasted in @After* methods for the correct displaying of step numbers in the tests
     */
    public static synchronized void removeNumberStep() {
        //numberStep.remove();
    }

    /**
     * Mark your logged message in red color that indicates the Fail
     *
     * @param message - message that should be mark as fail
     */
    public static void messageFail(String message) {
        TestReporter.fail(message);
        Assert.fail(message);
    }

    private static String getTimeStamp() {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        return format("Time %s", timestamp.toString());
    }
}
