package core.utility;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;
import core.utility.testrail.TestCase;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class TestSkipper implements IAnnotationTransformer {

    private String getCaseId(Method method, Class testClass) {
        //In case annotation will be placed on test method
        if (method!= null) {
            if (method.isAnnotationPresent(TestCase.class)) {
                return method.getAnnotation(TestCase.class).id();
            }
        }
        //In case annotation will be placed on test class
        if (testClass != null){
            if (testClass.isAnnotationPresent(TestCase.class)) {
                return ((TestCase)testClass.getAnnotation(TestCase.class)).id();
            }
        }
        return null;
    }

    @Override
    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        if (TestCaseManager.casesToRun != null) {
            annotation.setEnabled(
                    TestCaseManager.casesToRun.stream().anyMatch(str -> str.equals(getCaseId(testMethod, testClass))));
        }
    }
}
