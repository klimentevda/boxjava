package core.utility.testrail;

import org.json.simple.JSONObject;
import core.utility.PropertiesLoader;

import java.util.HashMap;
import java.util.Properties;

public class TestRailUtils {

    private static final String URL = "test.rail.url";
    private static final String USER = "test.rail.user";
    private static final String PASSWORD = "test.rail.password";

    static TestRailUtils instance;

    APIClient client;

    String url = "https://mangoerp.testrail.io";
    String user = "..";
    String password = "..";

    private TestRailUtils() {
    }

    public static String getURL() {
        return getInstance().url;
    }

    public static TestRailUtils getInstance() {
        if (instance == null) {
            instance = new TestRailUtils();

            Properties prop = PropertiesLoader.load();
            instance.url = prop.getProperty(URL);
            instance.user = prop.getProperty(USER);
            instance.password = prop.getProperty(PASSWORD);

            instance.updateAPIClient();
        }
        return instance;
    }

    //"index.php?/api/v2/add_result_for_case/{run_id}/{case_id}")
    public JSONObject addResultForCase(String runId, String caseId,
                                       int status, String description) {
        JSONObject c;
        HashMap data = new HashMap();
        data.put("status_id", new Integer(status));
        data.put("comment", description);
        c = (JSONObject) client.sendPost("add_result_for_case/" + runId + "/" + caseId, data);
        return c;
    }

    public JSONObject getTestCase(String caseId) {
        JSONObject c;
        c = (JSONObject) client.sendGet("get_case/" + caseId);
        return c;
    }

    public JSONObject getSection(String sectionId) {
        JSONObject c;
        c = (JSONObject) client.sendGet("get_section/" + sectionId);
        return c;
    }

    public JSONObject addTestRun(String projectId, String suiteId, String name, String description) {
        JSONObject result;
        JSONObject request = new JSONObject();

        HashMap data = new HashMap();
        data.put("name", name);
        data.put("description", description);
        data.put("suite_id", suiteId);

        request.putAll(data);
        result = (JSONObject) client.sendPost("add_run/" + projectId, request);
        return result;
    }

    public JSONObject closeRun(String runId) {
        JSONObject result;
        HashMap data = new HashMap();
        result = (JSONObject) client.sendPost("close_run/" + runId, data);
        return result;
    }

    public void updateAPIClient() {
        client = new APIClient(url);
        client.setUser(user);
        client.setPassword(password);
    }
}
