package core.utility.testrail;

import io.qameta.allure.SeverityLevel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent TestRail TestCase object
 */
public class TestCaseData {

    /** id of case **/
    private String id = "Test rail case id not provided.";
    /** title of case*/
    private String title = "";
    /** status index*/
    private long customStatus;
    /** status description*/
    private String status = "404";
    /** description of test case*/
    private String description = "";
    /** feature of test case*/
    private String feature = "";
    /** severity of test case*/
    private String severity = "";
    /** list of steps in test rail*/
    private List<Pair<String, String>> steps = new ArrayList<>();
    /** full information about case from rail*/
    private JSONObject caseObject;
    /** full information about case section from rail*/
    private JSONObject sectionObject;

    public void load(TestCase testCase) {
        if (testCase != null) {
            String id = testCase.id();
            if (id != null)
                load(id);
        }
    }

    public void load(String id) {
        this.id = id;
        caseObject = TestRailUtils.getInstance().getTestCase(id);
        //Get info about section in test rail
        String sectionId = caseObject.get("section_id").toString();
        sectionObject = TestRailUtils.getInstance().getSection(sectionId);
        feature = sectionObject.get("name").toString();
        //Add title
        title = caseObject.get("title").toString();
        //Add test case status

        if (caseObject.get("custom_test_case_status") != null) {
            customStatus = (long) caseObject.get("custom_test_case_status");
        }

        switch ((int) customStatus) {
            case 1:
                status = "Ready";
                break;
            case 2:
                status = "Automated";
                break;
            case 3:
                status = "Manual";
                break;
            case 4:
                status = "In Process";
                break;
            default:
                status = "Unknown";
                break;
        }

        long priorityId = (long) caseObject.get("priority_id");
        switch ((int) priorityId) {
            case 1:
                severity = SeverityLevel.MINOR.value();
                break;
            case 2:
                severity = SeverityLevel.NORMAL.value();
                break;
            case 3:
                severity = SeverityLevel.CRITICAL.value();
                break;
            case 4:
                severity = SeverityLevel.CRITICAL.value();
                break;
            case 5:
                severity = SeverityLevel.BLOCKER.value();
                break;
        }

        if (caseObject.get("custom_description") != null) {
            description = caseObject.get("custom_description").toString();
        } else {
            description = "";
            if (caseObject.get("custom_test_scenario") != null) {
                description = "\nTest Scenario:\n" + caseObject.get("custom_test_scenario").toString();
            }
        }

        if (caseObject.get("custom_steps_separated") != null) {
            JSONArray array = (JSONArray) caseObject.get("custom_steps_separated");
            for (Object object : array) {
                JSONObject obj = (JSONObject) object;
                String expected = obj.get("expected").toString();
                String content = obj.get("content").toString();


                Pair<String, String> pair = new Pair<>(content, expected);
                steps.add(pair);
            }
        }
    }

    public String getURL() {
        return TestRailUtils.getURL() + "/index.php?/cases/view/" + getId();
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public long getCustomStatus() {
        return customStatus;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public String getFeature() {
        return feature;
    }

    public String getSeverity() {
        return severity;
    }

    public List<Pair<String, String>> getSteps() {
        return steps;
    }

    public Object getCaseProperty(String key){
        return caseObject.get(key);
    }

    public Object getSectionProperty(String key){
        return sectionObject.get(key);
    }
}
