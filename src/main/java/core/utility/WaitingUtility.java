package core.utility;

import core.logger.MangoFinanceLogger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Alisa Demennikova
 */
public class WaitingUtility {
    private static final Logger LOGGER = MangoFinanceLogger.getMangoLogger().getLogger();
    private final static long WAITING_TIME_IN_SECONDS = 30;
    private final static int DELAY_BETWEEN_RETRIES_MILLIS = 1000;

    private WaitingUtility() {
    }

    public static void waitUntilAttributeNotContains(WebElement element, WebDriver driver, String attributeName, String attributeValue) {
        WebDriverWait wait = new WebDriverWait(driver, WAITING_TIME_IN_SECONDS, DELAY_BETWEEN_RETRIES_MILLIS * 3);
        wait.until(ExpectedConditions.not(ExpectedConditions.attributeContains(element, attributeName, attributeValue)));
    }

    public static WebElement elementVisibility(WebElement element, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, WAITING_TIME_IN_SECONDS);
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static List<WebElement> elementListVisibility(List<WebElement> elements, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, WAITING_TIME_IN_SECONDS);
        return wait.until(ExpectedConditions.visibilityOfAllElements(elements));
    }

    public static void waitForPageLoad(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, WAITING_TIME_IN_SECONDS);
        wait.until((ExpectedCondition<Boolean>) driver1 -> ((JavascriptExecutor) driver1).executeScript(
                "return document.readyState").equals("complete"));
    }

    public static WebElement waitForElementClickable(WebElement element, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, WAITING_TIME_IN_SECONDS);
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitInvisibilityOfElement(WebElement element, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, WAITING_TIME_IN_SECONDS);
        wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element)));
    }

    public static void waitInvisibilityIfElementExists(WebElement element, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, WAITING_TIME_IN_SECONDS);
        try {
            element.getLocation();
            wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element)));
        } catch (NoSuchElementException ignored) {
        }
    }

    public static WebElement waitForElementPresent(By element, WebDriver driver) {
        return (new WebDriverWait(driver, WAITING_TIME_IN_SECONDS))
                .until(ExpectedConditions.presenceOfElementLocated(element));
    }

    public static WebElement waitForElementPresent(WebElement element, WebDriver driver) {
        return (new WebDriverWait(driver, WAITING_TIME_IN_SECONDS))
                .until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForElementNotPresent(WebElement element, WebDriver driver) {
        new WebDriverWait(driver, WAITING_TIME_IN_SECONDS)
                .until(ExpectedConditions.stalenessOf(element));
    }

    public static void delay(final Integer timeoutInMilliSeconds) {
        try {
            Thread.sleep(timeoutInMilliSeconds);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
    }

    public static void waitFor(int timeOut) {
        delay(timeOut);
    }

    public static void waitForDynamicCondition(Callable<Boolean> condition,
                                               final Integer sleepInSeconds) {
        Long start = System.currentTimeMillis();
        Boolean result;
        while ((System.currentTimeMillis() - start) <= (sleepInSeconds * 1000)) {
            try {
                result = condition.call();
            } catch (Exception ex) {
                LOGGER.log(Level.WARNING, ex.getMessage());
                result = false;
            }
            if (!result) {
                delay(DELAY_BETWEEN_RETRIES_MILLIS);
            }
        }
    }
}