package core.utility.excel;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExcelLoader {

    public static HSSFWorkbook loadWorkbook(File file){
        try {
            FileInputStream fis = new FileInputStream(file);
            HSSFWorkbook workbook = new HSSFWorkbook(fis);
            //XSSFSheet sheet = workbook.getSheetAt(0);
            fis.close();
            return workbook;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static HSSFWorkbook loadWorkbook(String path){
        return loadWorkbook(new File(path));
    }

    public static HSSFSheet loadSheet(HSSFWorkbook workbook, String name){
        int index = workbook.getSheetIndex(name);
        if (index == -1) {
            throw new RuntimeException("Sheet named '" + name + "' not found in excel workbook");
        }
        return workbook.getSheetAt(index);
    }

    public static HSSFSheet loadSheet(HSSFWorkbook workbook, int index){
        return workbook.getSheetAt(index);
    }

    public static List<List<String>> loadSheetData(HSSFSheet sheet){
        List<List<String>> result = new ArrayList<>();
        for (Row row : sheet){
            List<String> cells = new ArrayList();
            result.add(cells);
            for (Cell cell : row){
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING : cells.add(cell.getStringCellValue());
                        break;
                    case Cell.CELL_TYPE_NUMERIC: cells.add(String.valueOf(cell.getNumericCellValue()));
                        break;
                    case Cell.CELL_TYPE_BOOLEAN: cells.add(String.valueOf(cell.getBooleanCellValue()));
                        break;
                    case Cell.CELL_TYPE_FORMULA: cells.add(String.valueOf(cell.getCellFormula()));
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        break;
                }
            }
        }
        return result;
    }

}
