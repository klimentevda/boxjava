package core.utility.excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import utils.FileUtils;

import java.util.List;

public class ExcelFile {

    private final HSSFWorkbook workbook;

    public ExcelFile(String fileName){
        String path = FileUtils.getPathFromResources(fileName);
        workbook = ExcelLoader.loadWorkbook(path);
    }

    public List<List<String>> getData(String sheetName){
        return ExcelLoader.loadSheetData(
                    ExcelLoader.loadSheet(workbook, sheetName)
            );
    }

    public void print(String sheetName){
        print(sheetName, 15);
    }

    public void print(String sheetName, int cellSize){
        List<List<String>> list = getData(sheetName);
        for (List<String> row : list ){
            System.out.println();
            for (String cell: row)
                System.out.printf("| %" + cellSize +  "." + cellSize +  "s |", cell);
        }
    }
}
