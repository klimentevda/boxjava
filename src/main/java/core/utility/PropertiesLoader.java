package core.utility;

import core.logger.MangoFinanceLogger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Level;

/**
 * @author Alisa Demennikova
 */
public class PropertiesLoader {
    private static final String DEFAULT_PROPERTIES_FILE_NAME = "config-DEMO.properties";
    private static final String PROPERTIES_FILE_NAME_TEMPLATE = "config-%s.properties";
    private static final String CONTOUR_NAME = "contour";

    private PropertiesLoader() {
    }

    public static Properties load() {
        Properties prop = new Properties();
        InputStream input = null;
        String propertyFile = DEFAULT_PROPERTIES_FILE_NAME;
        if (System.getProperty(CONTOUR_NAME) != null) {
            String contourName = System.getProperty(CONTOUR_NAME);
            MangoFinanceLogger.getMangoLogger().getLogger().log(Level.INFO, "Run tests on " + contourName);
            if (!contourName.isEmpty())
                propertyFile = String.format(PROPERTIES_FILE_NAME_TEMPLATE, contourName);
        }
        try {
            input = PropertiesLoader.class.getClassLoader().getResourceAsStream(propertyFile);
            if (input == null) {
                throw new RuntimeException("Unable TO find " + propertyFile);
            }
            prop.load(input);
        } catch (IOException ex) {
            throw new RuntimeException("Error during loading properties. See message for details: " + ex.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    throw new RuntimeException("Error during input closing. See message for details: " + e.getMessage());
                }
            }
        }
        return prop;
    }

    /**
     * @param name
     * @return property named.
     */
    public static Optional<String> getPropertie(String name) {
        Properties prop = load();
        return Optional.ofNullable(prop.getProperty(name));
    }
}
