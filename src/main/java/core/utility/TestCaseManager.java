package core.utility;

import java.util.Arrays;
import java.util.List;

public class TestCaseManager {
    public static List<String> casesToRun = getCaseIds();

    private static List<String> getCaseIds() {
        String cases = System.getenv("test");
        if (cases == null || cases.isEmpty()) {
            return null;
        }
        cases = cases.replace("*", "").replace("#", "").replace("C", "").replace("_", "");
        return Arrays.asList(cases.split("\\s*\\+\\s*"));
    }

}
