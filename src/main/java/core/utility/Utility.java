package core.utility;

import core.dataprovider.ExcelUtility;
import core.logger.MangoFinanceLogger;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.Range;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebElement;
import core.selenium.driver.WebDriverSettings;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static core.utility.Constants.*;

public class Utility {
    private static final Logger LOGGER = MangoFinanceLogger.getMangoLogger().getLogger();
    private static final String DATE_STRING_REGEX = "(\\d{1,2}[\\W]){1,2}\\d{2,4}";
    private static final String INTEGER_STRING_REGEX = "\\d+";
    private static final String EMAIL_REGEX = "^[A-Za-z0-9.%+\\-]+@[A-Za-z0-9.\\-]+\\.[A-Za-z]{2,4}";
    private static final String PHONE_NUMBER_REGEX = "(95|96|97)\\d{7}";

    private Utility() {
    }

    /**
     * Set Data for Excel cell by adjusted excel path, sheet, and column with row for particular test case
     *
     * @param sheetName - name of sheet in excel with needed data
     * @param colName   -   column with needed data
     * @param path      -      path to excel data file
     * @param rowNum    -    row with needed data
     * @param data      - data that must be setted to the cell
     */
    public static void setData(String sheetName, String colName, String path, int rowNum, String data) {
        ExcelUtility excel = new ExcelUtility(path);
        excel.setCellData(sheetName, colName, rowNum, data);
        LOGGER.log(Level.INFO, String.format("Test case starts FROM : %s", rowNum));
    }

    /**
     * Returns current date in setted through dateFormat input parameter format
     *
     * @return - formatted current date in a string
     */
    public static String getCurrentDate(String dateFormat) {
        Date parsedDate = now();
        DateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(parsedDate);
    }

    public static String getCurrentTime(String timeFormat) {
        Date parsedDate = now();
        DateFormat formatter = new SimpleDateFormat(timeFormat);
        return formatter.format(parsedDate);
    }

    private static Date now() {
        Date date = new Date();
        String currentDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date);
        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date parsedDate = null;
        try {
            parsedDate = formatter.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsedDate;
    }

    public static String addDaysToDate(String value, int day, String dateFormat) {
        if (!value.isEmpty()) {
            String targetDateFormat = formatDateFromOneToAnotherFormat
                    (value, dateFormat, "yyyy-MM-dd");
            return formatDateFromOneToAnotherFormat
                    (LocalDate.parse(targetDateFormat).plusDays(day).toString(),
                            "yyyy-MM-dd", dateFormat);
        }
        return value;
    }

    public static String getDayOfMonth(String value, String dateFormat) {
        if (!value.isEmpty()) {
            String targetDateFormat = formatDateFromOneToAnotherFormat
                    (value, dateFormat, "yyyy-MM-dd");
            LocalDate dateOfLocalDateFormat = LocalDate.parse(targetDateFormat);
            return Integer.toString(dateOfLocalDateFormat.getDayOfMonth());
        }
        return value;
    }

    public static String setMonth(int targetMonth) {
        String monthStringUK;
        try {
            monthStringUK = Month.of(targetMonth).getDisplayName(TextStyle.SHORT, Locale.UK);
        } catch (DateTimeException e) {
            monthStringUK = "No such month";
            LOGGER.log(Level.WARNING, String.format("DateTimeException, details message: %s", e.getMessage()));
        }
        return monthStringUK;
    }

    public static String setDecade(int targetYear) {
        if (Range.between(1900, 1911).contains(targetYear)) {
            return "1900 - 1911";
        } else if (Range.between(1912, 1923).contains(targetYear)) {
            return "1912 - 1923";
        } else if (Range.between(1924, 1935).contains(targetYear)) {
            return "1924 - 1935";
        } else if (Range.between(1936, 1947).contains(targetYear)) {
            return "1936 - 1947";
        } else if (Range.between(1948, 1959).contains(targetYear)) {
            return "1948 - 1959";
        } else if (Range.between(1960, 1971).contains(targetYear)) {
            return "1960 - 1971";
        } else if (Range.between(1972, 1983).contains(targetYear)) {
            return "1972 - 1983";
        } else if (Range.between(1984, 1995).contains(targetYear)) {
            return "1984 - 1995";
        } else if (Range.between(1996, 2007).contains(targetYear)) {
            return "1996 - 2007";
        } else if (Range.between(2000, 2011).contains(targetYear)) {
            return "2000 - 2011";
        } else if (Range.between(2012, 2023).contains(targetYear)) {
            return "2012 - 2023";
        } else if (Range.between(2024, 2035).contains(targetYear)) {
            return "2024 - 2035";
        } else if (Range.between(2036, 2047).contains(targetYear)) {
            return "2036 - 2047";
        } else if (Range.between(2048, 2059).contains(targetYear)) {
            return "2048 - 2059";
        } else if (Range.between(2060, 2071).contains(targetYear)) {
            return "2060 - 2071";
        } else if (Range.between(2072, 2083).contains(targetYear)) {
            return "2072 - 2083";
        } else if (Range.between(2084, 2095).contains(targetYear)) {
            return "2084 - 2095";
        } else if (Range.between(2096, 2107).contains(targetYear)) {
            return "2096 - 2107";
        } else {
            return "Year is out of range";
        }
    }

    public static String splitByRegexAndReturnOnlyFirstPart(String str, String regex) {
        String[] finalString = str.split(regex);
        return finalString[0];
    }

    public static List<String> splitStringToListByRegex(String string, String regex) {
        return Arrays.asList(string.split(regex));
    }

    public static void performFileUpload(WebDriver driver, String element) {
        WaitingUtility.waitFor(4000);
        WebElement uploadButton = driver.findElement(By.xpath(element));
        if (WebDriverSettings.getInstance().isUseRemoteDriver()) {
            ((RemoteWebElement) uploadButton).setFileDetector(new LocalFileDetector());
        }
        uploadButton.sendKeys(Constants.UPLOAD_FILE_PATH);
        WaitingUtility.waitFor(4000);
    }

    public static void performFileUpload(WebElement uploadInput) {
        WaitingUtility.waitFor(4000);
        uploadInput.sendKeys(Constants.UPLOAD_FILE_PATH);
        WaitingUtility.waitFor(4000);
    }

    public static String convertDateToGeneralFormat(String value) {
        String[] splitArray = value.split("/");
        if (splitArray[0].length() == 1) {
            splitArray[0] = "0" + splitArray[0];
        }
        if (splitArray[1].length() == 1) {
            splitArray[1] = "0" + splitArray[1];
        }
        if (splitArray[2].length() == 4) {
            splitArray[2] = splitArray[2].substring(2);
        }
        return String.format("%s/%s/%s", splitArray[0], splitArray[1], splitArray[2]);
    }

    public static String convertDateFromGeneralToAnotherFormat(String value) {
        Pattern pattern = Pattern.compile(DATE_STRING_REGEX);
        Matcher matcher = pattern.matcher(value);
        if (matcher.matches()) {
            try {
                DateFormat formatter = new SimpleDateFormat("dd/MM/yy");
                Date parsedDate = formatter.parse(value);
                formatter = new SimpleDateFormat("dd/MM/yyyy");
                return formatter.format(parsedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    public static String formatDateFromOneToAnotherFormat(String value, String givenFormat, String targetFormat) {
        SimpleDateFormat originalFormatter;
        SimpleDateFormat targetFormatter;
        try {
            originalFormatter = new SimpleDateFormat(givenFormat);
            targetFormatter = new SimpleDateFormat(targetFormat);
            return targetFormatter.format(originalFormatter.parse(value));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String convertDateToFormatBy(String formatDate, String date) {
        LocalDate dateOfLocalDateFormat = LocalDate.parse(date);
        DateTimeFormatter dTF = DateTimeFormatter.ofPattern(formatDate);
        return dTF.format(dateOfLocalDateFormat);
    }

    public static String addDotAndTwoZeroPlacesToStringValue(String value) {
        Pattern pattern = Pattern.compile(INTEGER_STRING_REGEX);
        Matcher matcher = pattern.matcher(value);
        if (matcher.matches()) {
            return value + ".00";
        } else return value;
    }

    public static int generateRandomNumberWithMinAndMaxBounds(int minBound, int maxBound) {
        if (minBound == maxBound)
            return minBound;

        int bound = maxBound - minBound;
        return minBound + new Random().nextInt(bound);
    }

    public static String generateRandomDateWithYearRange(int minYear, int maxYear) {
        int day = generateRandomNumberWithMinAndMaxBounds(MIN_DAY_MONTH_BOUND, MAX_DAY_BOUND);
        int month = generateRandomNumberWithMinAndMaxBounds(MIN_DAY_MONTH_BOUND, MAX_MONTH_BOUND);
        int year = generateRandomNumberWithMinAndMaxBounds(minYear, maxYear);
        return String.format("%d/%d/%d", day, month, year);
    }

    public static String generateRandomRussianAlphabet(int count) {
        return RandomStringUtils.random(count, "абвгдежзийклмнопрстуфхцчшъыьэюяАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЭЮЯ");
    }

    public static int getClientAge(int day, int month, int year) {
        LocalDate birthDate = LocalDate.of(year, month, day);
        LocalDate now = LocalDate.now();
        return calculateAge(birthDate, now);
    }

    public static int calculateAge(LocalDate birthDate, LocalDate currentDate) {
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

    public static String getSubstringCapturedByRegex(String regex, String string) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(string);
        if (matcher.find()) {
            return matcher.group();
        } else {
            throw new RuntimeException(String.format("String [%s] doesn't contain regex [%s]", string, regex));
        }
    }

    public static List<Integer> getRandomListOfIntegers(int bound, int numberOfElementsToCheck) {
        List<Integer> numbersGenerated = new ArrayList<>();
        for (int i = 0; i < numberOfElementsToCheck; i++) {
            Random randNumber = new Random();
            int iNumber = randNumber.nextInt(bound);
            if (!numbersGenerated.contains(iNumber)) {
                numbersGenerated.add(iNumber);
            } else {
                i--;
            }
        }
        return numbersGenerated;
    }

    public static String getRandomPhoneNumber() {
        return String.format(RANDOM_PHONE_NUMBER, RandomStringUtils.randomNumeric(6));
    }

    public static String getRandomPhoneNumber(String prefix, int suffixLength) {
        return String.format(prefix, RandomStringUtils.randomNumeric(suffixLength));
    }

    public static boolean dateIsWithinRange(Date dateToVerify, Date startDate, Date endDate) {
        return dateToVerify.getTime() >= startDate.getTime() &&
                dateToVerify.getTime() <= endDate.getTime();
    }

    public static boolean isInteger(WebElement element) {
        return element.getText().matches(INTEGER_STRING_REGEX);
    }

    public static boolean isEmail(WebElement element) {
        return element.getText().matches(EMAIL_REGEX);
    }

    public static boolean isPhoneNumber(WebElement element) {
        return element.getText().matches(PHONE_NUMBER_REGEX);
    }

    public static String getHexColor(WebElement webElement, String propertyName) {
        String rgb[] = webElement.getCssValue(propertyName).replaceAll(RGBA_TO_RGB_REGEX, "").split(COMMA_REGEX);
        return String.format("#%s%s%s", toBrowserHexValue(Integer.parseInt(rgb[0])), toBrowserHexValue(Integer.parseInt(rgb[1])),
                toBrowserHexValue(Integer.parseInt(rgb[2])));
    }

    private static String toBrowserHexValue(int number) {
        StringBuilder builder = new StringBuilder(Integer.toHexString(number & 0xff));
        while (builder.length() < 2) {
            builder.append("0");
        }
        return builder.toString();
    }
}
