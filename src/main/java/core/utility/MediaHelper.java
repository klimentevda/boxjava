package core.utility;

import core.logger.MangoFinanceLogger;
import org.apache.commons.io.FileUtils;
import org.monte.media.Format;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import core.selenium.driver.DriverManager;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.monte.media.FormatKeys.*;
import static org.monte.media.VideoFormatKeys.*;

public class MediaHelper {
    private static final Logger LOGGER = MangoFinanceLogger.getMangoLogger().getLogger();
    private static String sourceFile;

    /**
     * Store screenshots under test-output/html/screenshots folder
     *
     * @param fileName - first part of name of the captured screenshot
     * @return - full captured screenshot name
     */
    public static synchronized String captureScreenshot(String fileName) {
        String screenshotTime = Calendar.getInstance().getTime().toString().replaceAll(" ", "_").replaceAll(":", "-");
        String screenshotName = fileName + screenshotTime + ".png";
        File screenshotSrc = ((TakesScreenshot) DriverManager.currentDriver()).getScreenshotAs(OutputType.FILE);
        sourceFile = Constants.SCREENSHOT_PATH + screenshotName;
        try {
            FileUtils.copyFile(screenshotSrc, new File(sourceFile));
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, "Error during screenshot taking: " + e.getMessage());
        }
        return sourceFile;
    }

    /**
     * Method initiate process of recording video of test run
     *
     * @param testCase - currently executed test case name
     */
    public static void startRecording(String testCase) {
        File file = new File(Constants.SCREEN_VIDEO_PATH);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        ScreenRecorder screenRecorder;
        int width = screenSize.width;
        int height = screenSize.height;
        Rectangle captureSize = new Rectangle(0, 0, width, height);
        GraphicsConfiguration gc = GraphicsEnvironment
                .getLocalGraphicsEnvironment()
                .getDefaultScreenDevice()
                .getDefaultConfiguration();
        try {
            screenRecorder = new SpecializedScreenRecorder(gc, captureSize,
                    new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI),
                    new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
                            CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
                            DepthKey, 24, FrameRateKey, Rational.valueOf(15),
                            QualityKey, 1.0f,
                            KeyFrameIntervalKey, 15 * 60),
                    new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, "black",
                            FrameRateKey, Rational.valueOf(30)),
                    null, file, testCase);
            screenRecorder.start();
        } catch (IOException | AWTException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, String.format("Exception during starting video recording process: %s", e.getMessage()));
        }
        TestReporter.log(String.format("Start Screen Video recording for test case %s", testCase));
    }

    public static void stopRecording(ScreenRecorder screenRecorder) {
        try {
            screenRecorder.stop();
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, String.format("Exception during ending video recording process: %s", e.getMessage()));
        }
        TestReporter.log("Stop video recording");
    }
}
