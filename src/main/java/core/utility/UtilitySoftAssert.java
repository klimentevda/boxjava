package core.utility;

import org.testng.asserts.IAssert;

/**
 * @author Sergey Potapov
 */
public class UtilitySoftAssert extends org.testng.asserts.SoftAssert {

    @Override
    public void onAssertFailure(IAssert<?> assertCommand, AssertionError ex) {
        TestReporter.fail((assertCommand.getMessage() + TestReporter.getActualAndExpectedMessage(assertCommand.getActual().toString(),
                assertCommand.getExpected().toString())));
        super.onAssertFailure(assertCommand, ex);
    }

    @Override
    public void assertEquals(String actual, String expected, String message) {
        super.assertEquals(actual, expected, message);
    }

    @Override
    public void assertNotEquals(String actual, String expected, String message) {
        super.assertNotEquals(actual, expected, message);
    }

    @Override
    public void onAssertSuccess(IAssert<?> assertCommand) {
        TestReporter.success((assertCommand.getMessage() + TestReporter.getActualAndExpectedMessage(assertCommand.getActual().toString(),
                assertCommand.getExpected().toString())));
        super.onAssertSuccess(assertCommand);
    }
}