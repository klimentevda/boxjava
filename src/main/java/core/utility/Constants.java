package core.utility;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Constants {

    //EXCEL TEST DATA PATH
    public static final String EXCEL_PATH_DATA_PROVIDER = System.getProperty("user.dir") + "/MyFrame/src/test/resources/testdata";
    public static String UPLOAD_FILE_PATH = System.getProperty("user.dir") + "/MyFrame/src/main/resources/mock.txt";

    static Path fullyPath = Paths.get(System.getProperty("user.dir"));
    public static String pathToParentDirectory = fullyPath.getParent().toString();
    public static final String CHROME_DRIVER_PATH = pathToParentDirectory + "/MyFrame/src/main/resources/drivers/chromedriver.exe";

    //LOGGER FILES PATH
    public static final String LOGGING_HTML_FILE = "Logs/Logging.html";
    public static final String LOGGING_TXT_FILE = "Logs/Logging.txt";

    //AUTHENTICATION
    public static final String IE_AUTH_PATH_ZMB = System.getProperty("user.dir") + "/MyFrame/src/main/resources/authentication/WindowsAuthenticationIE.exe";

    //SCREENSHOT PATH
    public static final String RELATIVE_PROJECT_PATH_WITH_BACKSLASHES = System.getProperty("user.dir").replaceAll("\\\\", "/");
    public static final String EXCLUDE_SCREENSHOT_PATH = "/test-output/html/";
    public static final String SCREENSHOT_PATH = RELATIVE_PROJECT_PATH_WITH_BACKSLASHES + EXCLUDE_SCREENSHOT_PATH + "screenshots/";

    //REPORTS PATH
    public static final String REPORT_PATH = System.getProperty("user.dir") + "/ExtentReport/Reports/";

    //SCREEN VIDEO PATH
    public static final String SCREEN_VIDEO_PATH = System.getProperty("user.dir") + EXCLUDE_SCREENSHOT_PATH + "screenvideo/";

    //UTIL CONSTANTS
    public static final String ID = "id";
    public static final String XPATH = "xpath";
    public static final String YES = "Yes";
    public static final String NO = "No";
    public static final String ZERO_BEFORE_ONE_DIGIT_STRING_FORMAT = "0%s";
    public static final String ONE_BACKSLASH_REPLACEMENT = "$1";
    public static final int FIRST_RESULT = 0;
    public static final int SECOND_RESULT = 1;
    public static final int TWO_TABS_BROWSER = 2;
    public static final int THREE_TABS_BROWSER = 3;
    public static final String MANGO_OPTION = "Mango";
    public static final int FIRST_TAB_BROWSER = 0;
    public static final int THIRD_TAB_BROWSER = 2;
    public static final String EMPTY_STRING = "";
    public static final String ENABLED_STATUS = "Enabled";
    public static final String DISABLED_STATUS = "Disabled";
    public static final String RANDOM_PHONE_NUMBER = "9632%s";
    public static final String EMAIL_TEMPLATE = "%s@autotest.com";
    public static final String VALUE = "value";
    public static final String NAME = "name";
    public static final String XPATH_LABEL_FOR_WEB_ELEMENT = "preceding::label[1]";
    public static final String DATA_NODE_FOR_API_QUERY = "data";
    public static final String ID_NODE_FOR_API_QUERY = "id";
    public static final String TITLE_NODE_FOR_API_QUERY = "title";

    //PACKAGES
    public static final String TEST_CASES_PACKAGE_NAME = "testcases.";

    //SETTINGS CONSTANTS
    public static final String DOCUMENT_SERIES_COLUMN_NAME_DB = "document_series";
    public static final String DOCUMENT_NUMBER_COLUMN_NAME_DB = "document_number";
    public static final String EMAIL_IS_OPTIONAL_FIELD = "email_is_optional_field";
    public static final String FIRST_LOAN_ZERO_INTEREST_SETTING_KEY = "first_loan_zero_interest";
    public static final String APP_SET_REFRESH_TODO_TAB_SETTING_KEY = "app_set_refresh_todo_tab";

    //CLASSIFICATORS CONSTANTS
    public static final String EMPLOYMENT_STATUS = "employment_status";
    public static final String CONTACT_PERSON_RELATION_TYPE = "contact_person_relation_type";

    //REGEX
    public final static String GENERAL_DATE_VALUE_REGEX = "\\d{2}\\/\\d{2}\\/\\d{2}";
    public final static String ADDITIONAL_DATE_VALUE_REGEX = "\\d{2}\\/\\d{2}\\/\\d{4}";
    public static final String REPLACE_TWO_BACKSLASHES_WITH_ONE_REGEX = "(\\\\)\\\\";
    public static final String ONE_DIGIT_IN_NUMBER_REGEX = "^\\d{1}$";
    public static final String COMMA_REGEX = ",";
    public static final String SPACE_REGEX = " ";
    public static final String HYPHEN_REGEX = "-";
    public static final String DOUBLE_TO_INTEGER_REGEX = ".00";
    public static final String SINGLE_TO_INTEGER_REGEX = ".0";
    public static final String SEMICOLON_SPLIT_REGEX = "\\s*;\\s*";
    public static final String EXTRACT_ALL_DIGITS_REGEX = "\\d+";
    public static final String EXTRACT_ALL_DIGITS_AND_DEC_POINT_REGEX = "\\d+(\\.\\d+)*";
    public static final String DOUBLE_PERCENT_REGEX = "\\d+\\.\\d+\\%";
    public static final String ONE_LETTER_AT_THE_STRING_BEGIN_REGEX = "^[A-z]";
    public static final String TITLE_CONTAINER_EXTRACT_MAIN_ITEM_ID = "^[a-zA-z]*\\d+";
    public static final String ANY_NON_DIGIT_REGEX = "^\\D+";
    public static final String QUOTA_REGEX = "K";
    public static final String RUB_REGEX = "\u20BD";
    public static final String SHORT_NAME_REGEX = "[a-z]+\\s";
    public static final String SINGLE_WHITESPACE_REGEX = "\\s";
    public static final String NEW_LINE_REGEX = "\\n";
    public static final String FIRST_PART_DOT_REGEX = "\\.";
    public static final String RGBA_TO_RGB_REGEX = "(rgba)|(rgb)|(\\()|(\\s)|(\\))";
    public static final String EXPRESSION_IN_PARENTHESES_REGEX = "\\(.+\\)";
    public static final String BRACKETS_REGEX = "[\\[\\](){}]";
    public static final String LANGUAGE_REGEX = "[a-z]{2}";
    public static final String EMAIL_REGEX = "([a-z0-9]+(?:[._+-][a-z0-9]+)*)@([a-z0-9]+(?:[.-][a-z0-9]+)*[.][a-z]{2,})";


    //MESSAGES
    public static final String APPLICATIONS_MSG = "APPLICATIONS";
    public static final String LOANS_MSG = "LOANS";
    public static final String AGREEMENTS_MSG = "AGREEMENTS";
    public static final String IN_FIN_CALCULATOR_MSG = "in Financial Calculator";
    public static final String APPLICATION_LIST_MSG = "in Application list";
    public static final String IN_APPLICATION_MSG = "in Application";
    public static final String AGREEMENT_LIST_MSG = "in Agreement list";
    public static final String IN_AGREEMENT_MSG = "in Agreement";
    public static final String LOAN_LIST_MSG = "in Loan list";
    public static final String IN_LOAN_MSG = "in Loan";
    public static final String IN_INVOICE_MSG = "in Invoice";
    public static final String APP_CREATE_FORM_MSG = "in Application create form";
    public static final String IN_CLIENT_CARD_MSG = "in Client Card";
    public static final String IN_DATABASE_MSG = "in Database";
    public static final String IN_EXCEL_MSG = "in Excel";
    public static final String SUCCESSFUL_APP_MSG = "Application successfully created!";

    //NOTIFICATIONS STATUSES
    public static final String NEW_STATUS = "New";

    //TEMPLATES GROUPS
    public static final String APPLICATION_TEMPLATE_GROUP = "Applications";
    public static final String CLIENT_TEMPLATE_GROUP = "Clients";

    //EMAIL AND SMS TEMPLATES STATUSES
    public static final String NEW_APPLICATION_STATUS = "Application NEW status";
    public static final String CLIENT_CREATED_WITH_PASSWORD_STATUS = "Account created with password";

    //ATTRIBUTE NAMES
    public static final String HREF_ATTRIBUTE = "href";
    public static final String TITLE_ATTRIBUTE = "title";
    public static final String BORDER_COLOR_ATTRIBUTE = "border-color";
    public static final String BACKGROUND_COLOR_ATTRIBUTE = "background-color";
    public static final String TEXT_COLOR_ATTRIBUTE = "color";

    //TAG NAMES
    public static final String TD_TAG = "td";
    public static final String SPAN_TAG = "span";

    //VERIFY PARAMS
    public static final int NUMBER_OF_APPLICATIONS_TO_CHECK = 5;
    public static final int NUMBER_OF_PAGES_SHOULD_BE_CHECKED = 5;

    //DATE AND TIME
    public static final String DECADE = "Decade";
    public static final String DECADE_2000_2107 = "2000-2107";
    public static final String DECADE_1900_2007 = "1900-2007";
    public static final String MONTH = "Month";
    public static final String GENERAL_DATE_VALUE_FORMAT = "dd/MM/yy";
    public static final String GENERAL_TIME_VALUE_FORMAT = "HH:mm";
    public static final String FULL_TIME_VALUE_FORMAT = "HH:mm:ss";
    public static final String ADDITIONAL_DATE_VALUE_FORMAT = "dd/MM/yyyy";
    public static final int MIN_DAY_MONTH_BOUND = 1;
    public static final int MAX_DAY_BOUND = 28;
    public static final int MAX_MONTH_BOUND = 11;

    public enum Templates {
        ApplicationNEWStatusEmail(39),
        ApplicationNEWStatusSMS(17);

        private final Integer mask;

        Templates(Integer mask) {
            this.mask = mask;
        }

        public Integer getMask() {
            return mask;
        }
    }
}