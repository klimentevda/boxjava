package core.selenium.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import core.selenium.driver.listeners.IListener;

/**
 * Multiton class provide an access to web driver
 */
public class DriverManager {
    /**
     * Thread local holder of class
     **/
    private static ThreadLocal<DriverManager> driverThread = new ThreadLocal<DriverManager>();

    /**
     * Access to class object
     **/
    public static DriverManager getInstance() {
        if (driverThread.get() == null) {
            synchronized (DriverManager.class) {
                driverThread.set(new DriverManager());
            }
        }
        return driverThread.get();
    }

    /**
     * Return web driver existing for specific thread
     **/
    public static WebDriver currentDriver() {
        return getInstance().getDriver();
    }

    /**
     * @return true if web driver was initialized
     */
    public static boolean isRunned() {
        return getInstance().driver != null;
    }

    /**
     * Private version of event firing driver
     **/
    private EventFiringWebDriver driver;

    /**
     * return new instance of web driver if currently no driver instatiated
     **/
    protected synchronized WebDriver getDriver() {
        if (driver == null) {
            driver = DriverFactory.getInstance().getWebDriver();
        }
        return driver;
    }

    /**
     * turn off all listeners if {@param usePureWebDriver} true
     **/
    public void notUseListeners(boolean usePureWebDriver) {
        if (usePureWebDriver) {
            for (IListener listener : DriverFactory.getInstance().getListeners()) {
                driver.unregister(listener);
            }
        } else {
            for (IListener listener : DriverFactory.getInstance().getListeners()) {
                driver.register(listener);
            }
        }
    }

    /**
     * private constructor
     **/
    private DriverManager() {
    }

    /**
     * close web driver in current thread
     **/
    public synchronized void destroy() {
        if (driver != null) {
            getDriver().quit();
            driver = null;
        }
    }

    /**
     * Class destroyer
     **/
    @Override
    protected void finalize() throws Throwable {
        destroy();
        super.finalize();
    }

}
