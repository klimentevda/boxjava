package core.selenium.driver;

import core.utility.PropertiesLoader;

import java.util.Properties;

/**
 * Class containing settings required to
 */
public class WebDriverSettings {
    private static final String USE_REMOTE = "isGrid";
    private static final String REMOTE_URL = "remote.url";
    private static final String CONFIGURATION = "core.selenium.configuration";
    private static final String AJAX = "core.selenium.timeout.ajax";
    private static final String PAGE = "core.selenium.timeout.page";
    private static final String ELEMENT = "core.selenium.timeout.element";

    private static ThreadLocal<WebDriverSettings> driverThread = new ThreadLocal<WebDriverSettings>();

    public static WebDriverSettings getInstance() {
        if (driverThread.get() == null) {
            synchronized (WebDriverSettings.class) {
                driverThread.set(createInstance());
            }
        }
        return driverThread.get();
    }

    private static WebDriverSettings createInstance() {
        Properties prop = PropertiesLoader.load();
        WebDriverSettings settings = new WebDriverSettings();
        settings.useRemoteDriver = Boolean.valueOf(prop.getProperty(USE_REMOTE));
        settings.remoteUrl = prop.getProperty(REMOTE_URL);
        settings.browser = prop.getProperty(CONFIGURATION, "Chrome");
        settings.ajaxTimeout = Integer.valueOf(prop.getProperty(AJAX, "30"));
        settings.pageLoadTimeout = Integer.valueOf(prop.getProperty(PAGE, "60"));
        settings.elementWait = Integer.valueOf(prop.getProperty(ELEMENT, "10"));
        return settings;
    }

    /**
     * Set to true if used remote web driver
     */
    private boolean useRemoteDriver = false;
    /**
     * URL to be used in remote web driver
     */
    private String remoteUrl;
    /**
     * Name of configuration of browser
     */
    private String browser = "Chrome";
    /**
     * Timeout to be waiting of document ready state
     */
    private int pageLoadTimeout = 60;
    /**
     * Timeout to be waiting of ajax activities
     */
    private int ajaxTimeout = 30;
    /**
     * Timeout to access web elements
     */
    private int elementWait = 10;

    /**
     * @return true if remote web driver should be used.
     */
    public boolean isUseRemoteDriver() {
        return useRemoteDriver;
    }

    /**
     * Set to true if remote web driver should be used.
     */
    public void setUseRemoteDriver(boolean useRemoteDriver) {
        this.useRemoteDriver = useRemoteDriver;
    }

    /**
     * Return remote url - typically address of grid hub.
     */
    public String getRemoteUrl() {
        return remoteUrl;
    }

    /**
     * Set remote url
     */
    public void setRemoteUrl(String remoteUrl) {
        this.remoteUrl = remoteUrl;
    }

    /**
     * Get browser configuration name
     */
    public String getBrowser() {
        return browser;
    }

    /**
     * Set browser configuration name
     */
    public void setBrowser(String browser) {
        this.browser = browser;
    }

    /**
     * Get page load timeout
     */
    public int getPageLoadTimeout() {
        return pageLoadTimeout;
    }

    /**
     * Get ajax timeout
     */
    public int getAjaxTimeout() {
        return ajaxTimeout;
    }

    /**
     * Get element waiting timeout
     */
    public int getElementWait() {
        return elementWait;
    }

}