package core.selenium.driver.configurations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import core.selenium.driver.WebDriverSettings;

import java.net.MalformedURLException;
import java.net.URL;

@DriverConfiguration
public class FirefoxConfiguration implements IConfiguration {

    public WebDriver getDriver() {
        return new FirefoxDriver(getOptions());
    }

    public WebDriver getRemoteDriver() {

        try {
            return new RemoteWebDriver(new URL(WebDriverSettings.getInstance().getRemoteUrl()), getOptions());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public String getName() {
        return "Firefox";
    }

    public FirefoxOptions getOptions() {
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("start-maximized");
        options.addArguments("--disable-infobars");
        System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");
        return options;
    }
}
