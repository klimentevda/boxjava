package core.selenium.driver.configurations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import core.selenium.driver.WebDriverSettings;
import core.utility.Constants;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;

@DriverConfiguration
public class ChromeHeadlessConfiguration implements IConfiguration {

    public WebDriver getDriver() {
        return new ChromeDriver(getOptions());
    }

    public WebDriver getRemoteDriver() {

        try {
            return new RemoteWebDriver(new URL(WebDriverSettings.getInstance().getRemoteUrl()), getOptions());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public String getName() {
        return "ChromeHeadless";
    }

    public ChromeOptions getOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        options.addArguments("--disable-infobars");
        options.setHeadless(true);

        System.setProperty("webdriver.chrome.driver", Constants.CHROME_DRIVER_PATH);

        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
        options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);

        return options;
    }

}
