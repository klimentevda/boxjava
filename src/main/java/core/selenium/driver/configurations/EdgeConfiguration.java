package core.selenium.driver.configurations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import core.selenium.driver.WebDriverSettings;

import java.net.MalformedURLException;
import java.net.URL;

@DriverConfiguration
public class EdgeConfiguration implements IConfiguration {

    public WebDriver getDriver() {
        return new EdgeDriver(getOptions());
    }

    public WebDriver getRemoteDriver() {

        try {
            return new RemoteWebDriver(new URL(WebDriverSettings.getInstance().getRemoteUrl()), getOptions());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public String getName() {
        return "Edge";
    }

    public EdgeOptions getOptions() {
        //String driverPath = FileUtils.getFileFromResource("drivers\\MicrosoftWebDriver.exe").getAbsolutePath();
        //System.setProperty("webdriver.edge.driver", "");
        EdgeOptions options = new EdgeOptions();
        return options;
    }
}
