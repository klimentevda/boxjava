package core.selenium.driver.configurations;

import org.openqa.selenium.WebDriver;

public interface IConfiguration {

    WebDriver getDriver();

    WebDriver getRemoteDriver();

    String getName();
}
