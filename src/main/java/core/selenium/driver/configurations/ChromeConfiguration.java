package core.selenium.driver.configurations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import core.selenium.driver.WebDriverSettings;
import core.utility.Constants;

import java.net.MalformedURLException;
import java.net.URL;

@DriverConfiguration
public class ChromeConfiguration implements IConfiguration {

    public WebDriver getDriver() {
        return new ChromeDriver(getOptions());
    }

    public WebDriver getRemoteDriver() {

        try {
            return new RemoteWebDriver(new URL(WebDriverSettings.getInstance().getRemoteUrl()), getOptions());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public String getName() {
        return "Chrome";
    }

    public ChromeOptions getOptions() {
        ChromeOptions options = new ChromeOptions();

        //TO run on unix systems or from custom locations
        //System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + FileUtils.getSeparator()
        //        + "chromedriver"+ FileUtils.getExecutable());

        System.setProperty("webdriver.chrome.driver", Constants.CHROME_DRIVER_PATH);
        options.addArguments("start-maximized");
        options.addArguments("--disable-infobars");
        return options;
    }
}
