package core.selenium.driver.configurations;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import core.selenium.driver.WebDriverSettings;
import core.utility.Constants;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

@DriverConfiguration
public class ChromeContainerConfiguration implements IConfiguration {

    public WebDriver getDriver() {
        WebDriver driver = new ChromeDriver(getOptions());
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        return driver;
    }

    public WebDriver getRemoteDriver() {
        try {
            RemoteWebDriver remoteWebDriver = new RemoteWebDriver(new URL(WebDriverSettings.getInstance().getRemoteUrl()), getDesiredCapabilities());
            remoteWebDriver.setFileDetector(new LocalFileDetector());
            remoteWebDriver.manage().window().setSize(getResolution());
            remoteWebDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            return remoteWebDriver;
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    private Dimension getResolution() {
        return new Dimension(1920, 1080);
    }

    public String getName() {
        return "ChromeContainer";
    }

    public ChromeOptions getOptions() {
        ChromeOptions options = new ChromeOptions();
        System.setProperty("webdriver.chrome.driver", Constants.CHROME_DRIVER_PATH);
        options.addArguments("--disable-infobars");
        return options;
    }

    public DesiredCapabilities getDesiredCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("enableVNC", true);
        capabilities.merge(getOptions());
        return capabilities;
    }
}
