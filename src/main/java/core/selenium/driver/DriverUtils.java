package core.selenium.driver;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Utility class to separate components based on WebDriver from WebDriver manager.
 */
public class DriverUtils {

    /**
     * @return Selenium Java Script Executor
     */
    public static JavascriptExecutor javascriptExecutor() {
        return (JavascriptExecutor) DriverManager.getInstance().getDriver();
    }

    public byte[] takeScreenShot() {
        return ((TakesScreenshot) DriverManager.currentDriver()).getScreenshotAs(OutputType.BYTES);
    }

    /**
     * @return Selenium Actions Class
     */
    public static Actions actions() {
        return new Actions(DriverManager.getInstance().getDriver());
    }

    /**
     * @param seconds timeout in seconds.
     * @return WebDriverWait
     */
    public static WebDriverWait getWait(int seconds) {
        WebDriverWait wait = new WebDriverWait(DriverManager.getInstance().getDriver(),
                seconds);
        return wait;
    }

    /**
     * @return WebDriverWait with {@link WebDriverSettings} ajax wait
     */
    public static WebDriverWait getAjaxWait() {
        WebDriverWait wait = new WebDriverWait(DriverManager.getInstance().getDriver(),
                WebDriverSettings.getInstance().getAjaxTimeout());
        return wait;
    }

    /**
     * @return WebDriverWait with {@link WebDriverSettings} element wait
     */
    public static WebDriverWait getElementWait() {
        WebDriverWait wait = new WebDriverWait(DriverManager.getInstance().getDriver(),
                WebDriverSettings.getInstance().getElementWait());
        return wait;
    }

    /**
     * @return WebDriverWait with {@link WebDriverSettings} page wait
     */
    public static WebDriverWait getPageWait() {
        WebDriverWait wait = new WebDriverWait(DriverManager.getInstance().getDriver(),
                WebDriverSettings.getInstance().getPageLoadTimeout());
        return wait;
    }

    /**
     * @return Alert window
     */
    public static Alert getAlert() {
        return DriverManager.currentDriver().switchTo().alert();
    }
}
