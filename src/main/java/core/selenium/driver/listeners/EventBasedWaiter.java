package core.selenium.driver.listeners;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import core.selenium.driver.DriverManager;
import core.utility.ReflectionHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Waiter use classes that implements IWaiter interface and annotated with @EventWaiter
 */
public class EventBasedWaiter implements IListener {

   // private final static Logger loger = MangoFinanceLogger.getMangoLogger().getLogger();
    List<IWaiter> waiters;

    /**
     * Загружает все классы с аннотацией {@link EventWaiter}
     */
    private void loadSynchronizations() {
        waiters = new ArrayList<>();
        List<Class<?>> types = new ArrayList<>();
        for (Class<?> factoryType : ReflectionHelper.getAnnotatedWith(EventWaiter.class)) {
            EventWaiter eventWaiter = factoryType.getAnnotation(EventWaiter.class);
            if (eventWaiter.enabled()) {
                types.add(factoryType);
            }
        }
        Collections.sort(types, (o1, o2) -> {
            EventWaiter eventWaiter1 = o1.getAnnotation(EventWaiter.class);
            EventWaiter eventWaiter2 = o2.getAnnotation(EventWaiter.class);
            return Integer.compare(eventWaiter1.priority(), eventWaiter2.priority());
        });

        for (Class<?> factoryType : types) {
            IWaiter iWaiter;
            try {
                iWaiter = (IWaiter) factoryType.newInstance();
                waiters.add(iWaiter);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }

        //loger.log(Level.CONFIG, "Order of execution of IWaiter classes for event firing web driver:");
        //for (IWaiter waiter : waiters)
        //    loger.log(Level.CONFIG, " - " + waiter.getClass().getName());

    }

    /**
     * Конструктор
     */
    public EventBasedWaiter() {
        loadSynchronizations();
    }

    /**
     * Имя
     */
    public String getName() {
        return "Waiter";
    }

    private void sync() {
        DriverManager.getInstance().notUseListeners(true);
        for (IWaiter waiter : waiters)
            waiter.synchronize();
        DriverManager.getInstance().notUseListeners(false);
    }


    public void beforeAlertAccept(WebDriver webDriver) {
        sync();
    }

    public void afterAlertAccept(WebDriver webDriver) {
        sync();
    }

    public void afterAlertDismiss(WebDriver webDriver) {
        sync();
    }

    public void beforeAlertDismiss(WebDriver webDriver) {
        sync();
    }

    public void beforeNavigateTo(String s, WebDriver webDriver) {

    }

    public void afterNavigateTo(String s, WebDriver webDriver) {
        sync();
    }

    public void beforeNavigateBack(WebDriver webDriver) {
        sync();
    }

    public void afterNavigateBack(WebDriver webDriver) {
        sync();
    }

    public void beforeNavigateForward(WebDriver webDriver) {
        sync();
    }

    public void afterNavigateForward(WebDriver webDriver) {
        sync();
    }

    public void beforeNavigateRefresh(WebDriver webDriver) {
        sync();
    }

    public void afterNavigateRefresh(WebDriver webDriver) {
        sync();
    }

    public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {
        sync();
    }

    public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {
        sync();
    }

    public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
        sync();
    }

    public void afterClickOn(WebElement webElement, WebDriver webDriver) {
        sync();
    }

    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        sync();
    }

    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
        sync();
    }

    public void beforeScript(String s, WebDriver webDriver) {
        sync();
    }

    public void afterScript(String s, WebDriver webDriver) {
        sync();
    }

    public void beforeSwitchToWindow(String s, WebDriver webDriver) {
    //    sync();
    }

    public void afterSwitchToWindow(String s, WebDriver webDriver) {
        sync();
    }

    public void onException(Throwable throwable, WebDriver webDriver) {
        //log.debug("Selenium exception: ", throwable);
    }
}
