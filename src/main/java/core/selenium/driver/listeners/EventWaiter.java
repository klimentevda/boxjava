package core.selenium.driver.listeners;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mark class as Element Factory,
 * Class should implements {@link IWaiter}
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
public @interface EventWaiter {

    /**
     * if set to false class ill not be used
     **/
    boolean enabled() default true;

    /**
     * priority in order of executions. 100 by default
     **/
    int priority() default 100;
}
