package core.selenium.driver.listeners;

import org.openqa.selenium.support.events.WebDriverEventListener;

/**
 * Interface for creation of WebDriverEventListener
 */
public interface IListener extends WebDriverEventListener {

    /**
     * @return name of listener
     */
    String getName();
}
