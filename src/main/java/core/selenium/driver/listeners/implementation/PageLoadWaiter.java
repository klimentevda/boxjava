package core.selenium.driver.listeners.implementation;

import core.selenium.conditions.CustomConditions;
import core.selenium.driver.DriverUtils;
import core.selenium.driver.WebDriverSettings;
import core.selenium.driver.listeners.EventWaiter;
import core.selenium.driver.listeners.IWaiter;

@EventWaiter
/**
 * Класс ожидающий document.ReadyState
 */
public class PageLoadWaiter implements IWaiter {

    @Override
    public void synchronize() {
        DriverUtils
                .getWait(WebDriverSettings.getInstance().getPageLoadTimeout())
                .withMessage("Waiting for document.readyState to be in complete.")
                .until(CustomConditions.documentReadyStateComplete);
    }
}
