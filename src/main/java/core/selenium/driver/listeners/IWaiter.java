package core.selenium.driver.listeners;

/**
 * Interfaced used in event based waiter class,
 * that added to EventFiringWebDriver by default
 */
public interface IWaiter {

    /**
     * Function that will be perform before and after
     * almost each action of WebDriver
     */
    void synchronize();
}
