package core.selenium.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import core.selenium.driver.configurations.DriverConfiguration;
import core.selenium.driver.configurations.IConfiguration;
import core.selenium.driver.listeners.IListener;
import core.utility.ReflectionHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Factory used to create instance of WebDriver
 */
public class DriverFactory {
    /**
     * multi-ton instance
     **/
    private static ThreadLocal<DriverFactory> driverThread = new ThreadLocal<DriverFactory>();

    /**
     * Access point for multi-ton
     **/
    public static DriverFactory getInstance() {
        if (driverThread.get() == null) {
            synchronized (DriverFactory.class) {
                driverThread.set(new DriverFactory());
            }
        }
        return driverThread.get();
    }

    /**
     * Different browser configurations
     **/
    private Map<String, IConfiguration> browserConfigurations;
    /**
     * listeners
     **/
    private List<IListener> listeners;

    /**
     * Constructor
     **/
    private DriverFactory() {
        loadBrowserConfigurations();
        loadListeners();
    }

    /**
     * Load configurations of WebDriver that should be annotated with {@link DriverConfiguration}
     * and implement IConfiguration interface
     **/
    protected void loadBrowserConfigurations() {
        browserConfigurations = new HashMap();
        for (Class<?> confType : ReflectionHelper.getAnnotatedWith(DriverConfiguration.class)) {
            try {
                IConfiguration configuration = (IConfiguration) confType.newInstance();
                browserConfigurations.put(configuration.getName(), configuration);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        if (browserConfigurations.keySet().size() == 0)
            throw new RuntimeException("No configurations for core.selenium founded.");
    }

    /**
     * load listeners for EventFiringWebDriver.
     * Listeners should implement {@link IListener} interface and be placed in package of IListener.class
     */
    protected List<IListener> loadListeners() {
        listeners = new ArrayList<>();
        for (Class<?> listenerType : ReflectionHelper.getInstancesOf(IListener.class)) {
            try {
                IListener listener = (IListener) listenerType.newInstance();
                listeners.add(listener);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return listeners;
    }

    /**
     * @return Construct WebDriverWith all configurations and listeners
     */
    public EventFiringWebDriver getWebDriver() {
        WebDriver driver;
        String browser = WebDriverSettings.getInstance().getBrowser();

        if (WebDriverSettings.getInstance().isUseRemoteDriver()) {
            RemoteWebDriver remoteWebDriver = (RemoteWebDriver) browserConfigurations.get(browser).getRemoteDriver();
            //custom file detector
            remoteWebDriver.setFileDetector(new LocalFileDetector());
            driver = remoteWebDriver;
        } else {
            driver = browserConfigurations.get(browser).getDriver();
        }

        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
        for (IListener listener : listeners) {
            eventFiringWebDriver.register(listener);
        }
        return eventFiringWebDriver;
    }

    /**
     * @return all listeners
     */
    public List<IListener> getListeners() {
        return listeners;
    }

}
