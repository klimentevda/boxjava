package core.selenium.conditions;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import core.selenium.driver.DriverUtils;

/**
 * Provide custom conditions inteded to be used with DefaultWait and WebDriverWait
 */
public class CustomConditions {

    //private static final Log log = LoggerFactory.create(CustomConditions.class);

    /**
     * Check for document to have ready state
     */
    public static ExpectedCondition<Boolean> documentReadyStateInteractive = driver -> {
        try {
            String state = ((JavascriptExecutor) driver).executeScript("return document.readyState").toString();
            //log.debug("document.readyState = " + state);
            return state.equals("complete")
                    || state.equals("interactive");
        } catch (Exception e) {
            //log.debugShort("CustomConditions.documentReadyState = interactive: ", e);
            return true;
        }
    };

    /**
     * Check for document to have ready state
     */
    public static ExpectedCondition<Boolean> documentReadyStateComplete = driver -> {
        try {
            String state = ((JavascriptExecutor) driver).executeScript("return document.readyState").toString();
            //log.debug("document.readyState = " + state);
            return state.equals("complete");
        } catch (Exception e) {
            //log.debugShort("CustomConditions.documentReadyState = complete: ", e);
            return true;
        }
    };

    /**
     * Check jquery to finish it activities
     */
    public ExpectedCondition<Boolean> jQueryLoad = driver -> {
        try {
            String result = ((JavascriptExecutor) driver).executeScript("return jQuery.active == 0").toString();
            return result.equals("true");
        } catch (Exception e) {
            //log.debugShort("CustomConditions.jQueryLoad: ", e);
            return true;
        }
    };

    /**
     * Check angular to be loaded
     */
    public ExpectedCondition<Boolean> angularLoad = driver -> {
        try {
            String result = ((JavascriptExecutor) driver).executeScript
                    ("return (angular.element(document).injector().get('$http').pendingRequests.length === 0)").toString();
            return result.equals("true");
        } catch (Exception e) {
            //log.debugShort("CustomConditions.angularLoad: ", e);
            return true;
        }
    };

    /**
     * Wait for element to not exist by id
     * @param id
     * @return
     */
    public ExpectedCondition<Boolean> notExistByID(String id) {
        return driver -> {
            try {
                WebElement element = (WebElement)DriverUtils.javascriptExecutor().executeScript("return document.getElementById('"+ id +"');");
                return !element.isDisplayed();
            } catch (Exception e) {
                return true;
            }
        };
    }

    /**
     * Wait for element to be accessible
     * @param locator
     * @return
     */
    public ExpectedCondition<WebElement> accessible(By locator) {
        return driver -> {
            try {
                WebElement element = driver.findElement(locator);
                if (//element.isDisplayed() &&
                        element.isEnabled() &&
                                !element.getCssValue("visibility").equals("hidden")) {
                    return element;
                } else
                    return null;
            } catch (Exception e) {
                //log.debugShort("Exception during find element", e);
                return null;
            }
        };
    }

    /**
     * If element is present wait for custom condition in element css
     *
     * @param locator By locator of element
     * @param style   list of conditions separated by ; example: "visibility:none; opacity:0"
     * @return
     */
    public ExpectedCondition<Boolean> hasStyle(By locator, String style) {
        return webDriver -> hasStyles(webDriver, locator, style);
    }

    /**
     * Check presence of 'checkStyle' if 'ifStyle' present.
     *
     * @param locator
     * @param ifStyle
     * @param checkStyle
     * @return
     */
    public ExpectedCondition<Boolean> checkStyleIfOtherPresent(By locator, String ifStyle, String checkStyle) {
        return webDriver -> {
            if (hasStyles(webDriver, locator, ifStyle)) {
                return hasStyles(webDriver, locator, checkStyle);
            } else {
                return true;
            }
        };
    }

    private boolean hasStyles(WebDriver webDriver, By locator, String style) {
        try {
            boolean equals = false;
            WebElement element = webDriver.findElement(locator);
            String[] keyValues = style.split(";");
            for (String option : keyValues) {
                String key = option.trim().split(":")[0];
                String value = option.trim().split(":")[1];
                if (element.getCssValue(key).equals(value)) {
                    equals = true;
                    //log.debug(key + " have " + value);
                } else {
                    equals = false;
                    //log.debug(key + " expected " + value + " actual = " + element.getCssValue(key));
                }
            }

            if (equals) {
                return true;
            } else
                return false;

        } catch (NoSuchElementException e) {
            return true;
        } catch (Exception e) {
            //log.debugShort("Exception during find element", e);
            return false;
        }
    }
}
