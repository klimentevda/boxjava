package core.dataprovider;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import core.utility.Utility;

import java.lang.reflect.Type;
import java.util.*;

import static core.utility.Constants.ID_NODE_FOR_API_QUERY;

/**
 * @author Alisa Demennikova
 */
public class JsonParseHelper {
    /**
     * This method assumes that json would have next format:
     * {
     * "upper_level_node": [
     * {"sub-node1": "sub-node value"},
     * {"sub-node2": "sub-node value"}  ]
     * }
     * If json format NOT as -> Json object that contains "first_level_node" and then json array [], then method gets
     * result of "first_level_node" and then finds value of innerNodeNameWhichDataNeeded and represents it as a single
     * element in result list. In other cases (f.e. when parsed json structure is different, or when sub-node contains
     * one more json array), method would represents all array as a simple String in the list "innerList"
     *
     * @param jsonString                   - string representation of parsed json response
     * @param firstLevelNodeName           - upper level node
     * @param innerNodeNameWhichDataNeeded - direct node, which data you want to parse to the list of strings
     * @return - list of strings that contains all data occurrences by node name
     */
    public static List<String> parseJsonByNodeAndSubNodeName(String jsonString, String firstLevelNodeName,
                                                             String innerNodeNameWhichDataNeeded) {
        List<String> innerList = new ArrayList<>();
        for (List<String> list : getDataByNodesNameInner(jsonString, firstLevelNodeName, innerNodeNameWhichDataNeeded)) {
            for (String s : list) {
                innerList.add(s);
            }
        }
        return innerList;
    }

    /**
     * Gets Json node at the first level, and then parses every sub-nodes inside this node by particular node name,
     * returns list of list of strings
     *
     * @param jsonString                   - string representation of parsed json response
     * @param firstLevelNodeName           - upper level node
     * @param innerNodeNameWhichDataNeeded - direct node, which data you want to parse to the list of strings
     * @return - list of list of strings that contains all data occurrences by node name
     */
    private static List<List<String>> getDataByNodesNameInner(String jsonString, String firstLevelNodeName,
                                                              String innerNodeNameWhichDataNeeded) {
        JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
        JsonArray testProperties;
        List<List<String>> resultData = new ArrayList<>();
        try {
            testProperties = getTestPropertiesFromJson(firstLevelNodeName, jsonObject, innerNodeNameWhichDataNeeded);
        } catch (ClassCastException e) {
            resultData.add(Collections.singletonList(jsonObject.getAsJsonObject(firstLevelNodeName).get(innerNodeNameWhichDataNeeded)
                    .toString().replaceAll("\"", "")));
            return resultData;
        }
        for (int i = 0; i < testProperties.size(); i++) {
            resultData.add(getValuesFromPropertiesByNodeName(testProperties.get(i), innerNodeNameWhichDataNeeded));
        }
        return resultData;
    }

    /**
     * Wrapped method above {@link JsonObject#getAsJsonArray(String)} method
     *
     * @param firstLevelNodeName - upper level node
     * @param jsonObject         - parsed json object
     * @return - JsonArray object
     */
    private static JsonArray getTestPropertiesFromJson(String firstLevelNodeName, JsonObject jsonObject, String subNode) throws ClassCastException {
        return jsonObject.getAsJsonArray(firstLevelNodeName);
    }

    /**
     * Converts JsonElement object to List of Strings
     *
     * @param jsonElement - JsonElement object that need to be converted
     * @param nodeName    - particular node name, which data is in need
     * @return - List of Strings
     */
    private static List<String> getValuesFromPropertiesByNodeName(JsonElement jsonElement, String nodeName) {
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> tempMap = new Gson().fromJson(jsonElement, type);
        List<String> valuesOfNode = new ArrayList<>();
        for (Map.Entry<String, String> entry : tempMap.entrySet()) {
            if (entry.getKey().equals(nodeName)) {
                valuesOfNode.add(entry.getValue());
            }
        }
        return valuesOfNode;
    }

    public static List<String> getIdAndDataOfFirstElementJsonArray(String jsonString, String innerNodeNameWhichDataNeeded) {
        JsonArray jArray = new JsonParser().parse(jsonString).getAsJsonArray();
        int randomIndex = Utility.generateRandomNumberWithMinAndMaxBounds(0, jArray.size() - 1);
        return Arrays.asList(jArray.get(randomIndex).getAsJsonObject().get(ID_NODE_FOR_API_QUERY).toString(),
                jArray.get(randomIndex).getAsJsonObject().get(innerNodeNameWhichDataNeeded).toString());
    }

}
