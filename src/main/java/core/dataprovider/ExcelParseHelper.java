package core.dataprovider;

import core.logger.MangoFinanceLogger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import core.utility.Constants;
import core.utility.TestReporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC;
import static core.utility.Constants.GENERAL_DATE_VALUE_FORMAT;

/**
 * @author Alisa Demennikova
 */
public class ExcelParseHelper {
    private static final Logger LOGGER = MangoFinanceLogger.getMangoLogger().getLogger();

    // ------- REGEXP -------
    private static final String INTEGER_PATTERN = "[.][0]$";
    private static final String INTEGER_FORMAT_FIELDS = "number|payDate|age|term";

    // ------- FORMATS -------
    private static final String TWO_DECIMAL_PLACES_FORMAT = "0.00";
    private static DecimalFormat f = new DecimalFormat(TWO_DECIMAL_PLACES_FORMAT);

    // ------- COLUMN NAMES -------
    private static final String LOAN_AMOUNT = "loanAmount";
    private static final String APR = "APR";
    private static final String DAILY = "daily";
    private static final String MONTHLY_INTEREST_RATE = "monthlyInterestRate";
    private static final String MONTHLY_ROLLOVER_RATE = "monthlyRolloverRate";
    private static final String ROLLOVER_DAYS = "rolloverDays";
    private static final String ROLLOVER_RATE = "rolloverRate";
    private static final String INTEGER_VALUE_IN_DOUBLE_PATTERN = ".0";

    private ExcelParseHelper() {
    }

    /**
     * @param testMethod - invocation test method
     * @return array of test data from excel files located under /test/resources/testdata/
     * @throws IOException
     */
    public static synchronized String[][] getConfiguredData(Method testMethod) throws IOException {
        return getDataForClassName(testMethod.getDeclaringClass().getName(), testMethod.getName(), testMethod.getParameterTypes().length);
    }

    /**
     * @param className          - name of class that contains certain invoked test-method
     * @param methodName         - invoked test-method name
     * @param numberOfParameters - number of parameters that invoked method passes
     * @return
     * @throws IOException
     */
    private static String[][] getDataForClassName(String className, String methodName, int numberOfParameters)
            throws IOException {
        return fillData(getDataSheetForClassName(className, methodName), numberOfParameters);
    }

    /**
     * @param sheet           - concrete Excel data file Sheet
     * @param numberOfColumns - number of colons in concrete data sheet
     * @return String[][] of test data from Excel file
     */
    private static String[][] fillData(Sheet sheet, int numberOfColumns) {
        return fillData(sheet, 1, numberOfColumns);
    }

    /**
     * Filling data to String [][] array
     *
     * @param sheet       - concrete Excel data file Sheet
     * @param startColumn - always 1 (hardcoded in {@link ExcelParseHelper#fillData(Sheet, int)}) - appears as a variable
     *                    to avoid confusions with hardcoded values inside method body
     * @param endColumn   - as usual summary number of columns in Excel file Sheet
     * @return String[][] of test data from Excel file
     */
    private static String[][] fillData(Sheet sheet, int startColumn, int endColumn) {
        if (startColumn == 0) {
            LOGGER.log(Level.WARNING, "Start column value should begin from 1");
            throw new RuntimeException("Start column value should begin from 1.");
        }
        if (endColumn < startColumn) {
            LOGGER.log(Level.WARNING, "End column number cannot be less than start column number");
            throw new RuntimeException("End column number cannot be less than start column number.");
        }
        int rows = sheet.getPhysicalNumberOfRows();
        int numberOfParameters = endColumn - startColumn + 1;
        String[][] returnValue = new String[rows - 1][numberOfParameters];
        LOGGER.log(Level.INFO, "TEST: -> " + sheet.getSheetName());
        Row name = sheet.getRow(0);
        for (int row = 1; row < rows; row++) {
            Row currentRow = sheet.getRow(row);
            int jj = 0;
            for (int currentCell = startColumn - 1; currentCell < endColumn; currentCell++) {
                int cellType;
                String colName;
                try {
                    colName = handleCell(name.getCell(currentCell).getCellType(), name.getCell(currentCell), "");
                    cellType = currentRow.getCell(currentCell).getCellType();
                } catch (NullPointerException ex) {
                    LOGGER.info("NullPointerException occurs ");
                    returnValue[row - 1][jj] = "";
                    continue;
                }
                returnValue[row - 1][jj] = handleCell(cellType, currentRow.getCell(currentCell), colName);
                LOGGER.log(Level.INFO, "Iteration ---> " + row + " COLUMN: -> " + colName + " - DATA VALUE: -> " +
                        returnValue[row - 1][jj]);
                jj++;
            }
        }
        return returnValue;
    }

    /**
     * Processes Cells inside the data Sheet in accordance with their contents
     *
     * @param cellType - as usual - Integer value from 0 to 4 that corresponds to certain cell type
     * @param cell     - concrete Cell that must be processed
     * @return - correctly displayed value wrote to the String type
     */
    private static String handleCell(int cellType, Cell cell, String cellName) {
        String value;
        String temp;
        switch (cellType) {
            case CELL_TYPE_NUMERIC:
                value = numericCellParser(cell, cellName);
                break;
            case Cell.CELL_TYPE_STRING:
                if (cellName.contains(ROLLOVER_RATE)) {
                    temp = replaceCommaWithDotForNumberInString(cell.getStringCellValue());
                } else {
                    temp = cell.getStringCellValue();
                }
                value = temp;
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                value = Boolean.toString(cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_BLANK:
            case Cell.CELL_TYPE_ERROR:
            default:
                value = "";
                break;
        }
        return value;
    }

    /**
     * Method created just to separate logic. Parses data for numerical data type
     *
     * @param cell - member of Cell class - concrete Cell that needed to be verify
     * @return - parsed string according to suitable format
     */
    private static String numericCellParser(Cell cell, String cellName) {
        if (DateUtil.isCellDateFormatted(cell)) {
            return parseDate(cell.getDateCellValue());
        } else {
            return parseNumber(cell, cellName);
        }
    }

    private static String parseDate(Date date) {
        DateFormat df = new SimpleDateFormat(GENERAL_DATE_VALUE_FORMAT);
        return df.format(date);
    }

    private static String parseNumber(Cell cell, String cellName) {
        try {
            String cellValue = cell.toString();
            if (cellName.matches(INTEGER_FORMAT_FIELDS) || cellName.contains(ROLLOVER_DAYS) || cellName.contains(LOAN_AMOUNT)) {
                return castStringValueToIntegerFormat(cellValue);
            } else if (cellName.contains(ROLLOVER_RATE)) {
                if (cellValue.contains(INTEGER_VALUE_IN_DOUBLE_PATTERN)) {
                    return castStringValueToIntegerFormat(cellValue);
                } else return replaceCommaWithDotForNumberInString(cellValue);
            } else if (cellName.contains(MONTHLY_INTEREST_RATE) || cellName.contains(MONTHLY_ROLLOVER_RATE)) {
                double rate = Double.valueOf(cellValue);
                return replaceCommaWithDotForNumberInString(String.format("%.2f", rate));
            } else if (cellName.equals(APR) || cellName.contains(DAILY)) {
                return roundToTwoDecimalPlacesAndFormat(cell.getNumericCellValue());
            } else {
                return replaceCommaWithDotForNumberInString(f.format(Math.round(cell.getNumericCellValue())));
            }
        } catch (NumberFormatException ex) {
            return "";
        }
    }

    private static String roundToTwoDecimalPlacesAndFormat(double value) {
        return replaceCommaWithDotForNumberInString(f.format((double) Math.round(value * 100) / 100));
    }

    private static String castStringValueToIntegerFormat(String value) {
        return value.replaceAll(INTEGER_PATTERN, "");
    }

    private static String replaceCommaWithDotForNumberInString(String value) {
        return value.replace(",", ".");
    }

    /**
     * Compiles Excel data file full path and name, after this, loads this file and get concrete Sheet
     *
     * @param className - name of class in which certain method executes
     * @param sheetName - must be same as invoking method name
     * @return - Sheet from Excel file that matches by name to invoked test name
     * @throws IOException
     */
    private static Sheet getDataSheetForClassName(String className, String sheetName) throws IOException {
        Pattern dotPattern = Pattern.compile("(\\W)");
        String[] separatedArrayOfClassName = dotPattern.split(className);
        List<String> classNamesList = Arrays.asList(separatedArrayOfClassName);
        String shortenClassName = classNamesList.get(separatedArrayOfClassName.length - 1);

        String dataFileName = String.format("%s/%s.xlsx", Constants.EXCEL_PATH_DATA_PROVIDER, shortenClassName).replace("/", File.separator);
        TestReporter.fail("Data file location = " + dataFileName);
        return getDataSheet(new File(dataFileName), sheetName);
    }

    /**
     * Loads excel file, parses by .xls or .xlsx name ending
     *
     * @param dataFile  - excel file
     * @param sheetName - must be same as invoking method name
     * @return Excel Sheet according to sheet name
     * @throws IOException
     */
    private static Sheet getDataSheet(File dataFile, String sheetName) throws IOException {
        Workbook workBook = null;
        InputStream inputStream = new FileInputStream(dataFile);

        if (dataFile.getName().toLowerCase().endsWith("xlsx")) {
            workBook = new XSSFWorkbook(inputStream);
        } else if (dataFile.getName().toLowerCase().endsWith("xls")) {
            workBook = new HSSFWorkbook(inputStream);
        }

        inputStream.close();
        if (workBook == null) {
            LOGGER.log(Level.WARNING, "Unable to create a Work Book for " + dataFile.getName());
            throw new IOException("Unable to create a Work Book for " + dataFile.getName());
        }
        Sheet sheet = workBook.getSheet(sheetName);
        if (sheet == null) {
            LOGGER.log(Level.WARNING, "Data sheet [" + sheetName + "] not found in data file -> " + dataFile.getName());
            throw new NullPointerException("Data sheet [" + sheetName + "] not found in data file -> " + dataFile.getName());
        }
        workBook.close();
        return sheet;
    }
}