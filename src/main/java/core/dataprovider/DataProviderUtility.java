package core.dataprovider;

import core.logger.MangoFinanceLogger;
import org.testng.annotations.DataProvider;
import core.utility.TestReporter;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Alisa Demennikova
 */
public class DataProviderUtility {
    private static final Logger LOGGER = MangoFinanceLogger.getMangoLogger().getLogger();

    private DataProviderUtility() {
    }

    /**
     * Method is a Data Provider of test data to a certain test
     *
     * @param method - method that is executed at the moment
     * @return array of data that should be passed to particular test
     */
    @DataProvider(name = "dataWithConfig")
    public static synchronized Object[][] getTestData(Method method) {
        Object[][] testData = null;
        try {
            testData = ExcelParseHelper.getConfiguredData(method);
        } catch (IOException e) {
            TestReporter.fail("TestDataProvider. Exception caught while receiving test data");
            LOGGER.log(Level.WARNING, "TestDataProvider. Exception caught while receiving test data" + e.getMessage());
        }
        return testData;
    }
}
