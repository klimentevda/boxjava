package core.dataprovider;

import core.logger.MangoFinanceLogger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import core.utility.TestReporter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Armands Bruns
 */
public class ExcelUtility {
    private static final Logger LOGGER = MangoFinanceLogger.getMangoLogger().getLogger();
    /**
     * The path of the referenced file
     * (null if the stream is created with a file descriptor)
     */
    private String path;
    private FileInputStream fis = null;
    private XSSFWorkbook workbook = null;
    private XSSFSheet sheet = null;
    private XSSFRow row = null;
    private XSSFCell cell = null;

    /**
     * Creates a FileInputStream by
     * opening a connection to an actual file,
     * If the named file does not exist, is a directory rather than a regular
     * file, or for some other reason cannot be opened for reading then a
     * FileNotFoundException is thrown.
     * Get the XSSFSheet object at the given index.
     * Closes this file input
     *
     * @param path - to test data file
     * @throws FileNotFoundException - if the file does not exist
     *                               SecurityException - a security manager exists and denies to read the file
     */
    public ExcelUtility(String path) {
        this.path = path;
        try {
            fis = new FileInputStream(path);
            workbook = new XSSFWorkbook(fis);
            sheet = workbook.getSheetAt(0);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, "Unable to parse EXCEL file with path" + path + " " + e.getMessage());
        }
    }

    /**
     * index - returns the index of the sheet by his name
     * sheet - get the XSSFSheet object at the given index
     * row - returns the logical row.  If you ask for a row that is not defined you get a null. This is to say row 4 represents the fifth row on a sheet.
     * cell - returns the cell at the given index (colNum)
     * check the cell type - cells can be numeric, blank, date, formula-based or string-based (text)
     * <p>
     * String cells - returns the value of the cell as a string
     * Formula cells - return the value of the cell as a string
     * Numeric cells - get the value of the cell as a number and returns the value of the cell as a string
     * Date cells - get the value of the cell as a number and returns the value of the given calendar field in such date format 'DD/MM/YY' as a string
     * Blank cells - return the value of the cell as an empty string
     * other case - return the value of the cell (boolean value) as a string
     *
     * @param sheetName - name of the sheet, cell data of which should be found out
     * @param colNum    - number of the column, cell data of which should be found out
     * @param rowNum    - number of the row, cell data of which should be found out
     * @return returns the data from a cell
     * @throws Exception row or column does not exist in excel sheet
     */
    public String getCellData(String sheetName, int colNum, int rowNum) {
        try {
            if (rowNum <= 0) {
                LOGGER.log(Level.WARNING, String.format("Excel row number for %s sheet is less than 0", sheetName));
                return "";
            }
            int index = workbook.getSheetIndex(sheetName);
            if (index == -1) {
                return "";
            }
            sheet = workbook.getSheetAt(index);
            row = sheet.getRow(rowNum - 1);
            if (row == null) {
                return "";
            }
            cell = row.getCell(colNum);
            if (cell == null) {
                return "";
            }
            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                return cell.getStringCellValue();
            } else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
                cell.setCellType(Cell.CELL_TYPE_STRING);
                return cell.getStringCellValue();
            } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                String cellText = String.valueOf(cell.getNumericCellValue());
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    double d = cell.getNumericCellValue();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(HSSFDateUtil.getJavaDate(d));
                    cellText = (String.valueOf(cal.get(Calendar.YEAR)));
                    cellText = cal.get(Calendar.MONTH) + 1 + "/" + (cal.get(Calendar.DAY_OF_MONTH)) + "/" + cellText;
                }
                return cellText;
            } else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                return "";
            } else {
                return String.valueOf(cell.getBooleanCellValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, String.format("Row %s or column %s does not exist in excel sheet", rowNum, colNum));
            return String.format("Row %s or column %s does not exist in excel sheet", rowNum, colNum);
        }
    }

    /**
     * Creates a FileInputStream by
     * opening a connection to an actual file,
     * index - returns the index of the sheet by his name
     * sheet - get the XSSFSheet object at the given index
     * row - returns the logical row.  If you ask for a row that is not defined you get a null. This is to say row 4 represents the fifth row on a sheet.
     * getLastCellNum - gets the index of the last cell contained in this row plus one. The result also
     * happens to be the 1-based column number of the last cell
     * Get the value of the cell as a string removed whitespace and compares this string to the specified colName
     * cell - returns the cell at the given index (colNum)
     * data - set a string value for the cell.
     * fileOut - creates a file output stream to write to the file with the specified name
     * Write out this document to an Out put stream
     * Closes this file output stream and releases any system resources
     *
     * @param sheetName - name of the sheet, cell data which should be write to specify the cell
     * @param colName   - name of the column, cell data which should be write to specify the cell
     * @param rowNum    - number of the row, cell data which should be write to specify the cell
     * @param data      - string value for the cell
     * @return true if data is set successfully else false
     * @throws Exception test data for sheet was not successfully set
     * @throws Exception test data for sheet was not successfully set
     */
    public boolean setCellData(String sheetName, String colName, int rowNum, String data) {
        try {
            fis = new FileInputStream(path);
            workbook = new XSSFWorkbook(fis);
            if (rowNum <= 0) {
                LOGGER.log(Level.WARNING, String.format("Excel row number for %s sheet is less than 0", sheetName));
                return false;
            }
            int index = workbook.getSheetIndex(sheetName);
            int colNum = -1;
            if (index == -1) {
                return false;
            }
            sheet = workbook.getSheetAt(index);
            row = sheet.getRow(0);
            for (int i = 0; i < row.getLastCellNum(); i++) {
                if (row.getCell(i).getStringCellValue().trim().equals(colName)) {
                    colNum = i;
                }
            }
            if (colNum == -1) {
                return false;
            }
            sheet.autoSizeColumn(colNum);
            row = sheet.getRow(rowNum - 1);
            if (row == null) {
                row = sheet.createRow(rowNum - 1);
            }
            cell = row.getCell(colNum);
            if (cell == null) {
                cell = row.createCell(colNum);
            }
            cell.setCellValue(data);
            TestReporter.log(colName + " - " + data);
            FileOutputStream fileOut = new FileOutputStream(path);
            workbook.write(fileOut);
            fileOut.close();

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, String.format("Test data %s for %s sheet was NOT successfully set", data, sheetName));
            return false;
        }
        return true;
    }
}
