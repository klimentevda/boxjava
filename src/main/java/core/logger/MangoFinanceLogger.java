package core.logger;

import java.io.File;
import java.io.IOException;
import java.util.logging.*;

import static core.utility.Constants.LOGGING_HTML_FILE;
import static core.utility.Constants.LOGGING_TXT_FILE;

/**
 * @author Demennikova Alisa
 */
public class MangoFinanceLogger {
    private static final MangoFinanceLogger MANGO_FINANCE_LOGGER = new MangoFinanceLogger();
    private Logger logger;

    private MangoFinanceLogger() {
        setup();
    }

    public static MangoFinanceLogger getMangoLogger() {
        return MANGO_FINANCE_LOGGER;
    }

    public Logger getLogger() {
        return logger;
    }

    /**
     * Get the global core.logger to configure it
     * Suppress the logging output to the console
     * Create a TXT formatter
     * Create an HTML formatter
     */
    private void setup() {
        logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        Formatter formatterHTML = new HtmlLoggerFormatter();
        SimpleFormatter formatterTxt = new SimpleFormatter();
        FileHandler fileTxt;
        FileHandler fileHTML;

        Logger rootLogger = Logger.getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        if (handlers.length != 0) {
            if (handlers[0] instanceof ConsoleHandler) {
                rootLogger.removeHandler(handlers[0]);
            }
        }
        logger.setUseParentHandlers(false);
        logger.setLevel(Level.ALL);
        try {

            File dir = new File("Logs");
            if (!dir.exists())
                dir.mkdir();

            fileTxt = new FileHandler(LOGGING_TXT_FILE, true);
            fileHTML = new FileHandler(LOGGING_HTML_FILE, true);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to create log files!");
        }
        fileTxt.setFormatter(formatterTxt);
        logger.addHandler(fileTxt);

        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.INFO);
        logger.addHandler(consoleHandler);

        fileHTML.setFormatter(formatterHTML);
        logger.addHandler(fileHTML);
    }
}
