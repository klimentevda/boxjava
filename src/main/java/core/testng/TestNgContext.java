package core.testng;

public class TestNgContext {

    private static ThreadLocal<String> currentTest = new ThreadLocal<>();

    public static void setCurrentTestId(String id) {
        currentTest.set(id);
    }

    public static String getCurrentTestId() {
        return currentTest.get();
    }
}
