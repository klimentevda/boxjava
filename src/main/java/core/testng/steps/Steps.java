package core.testng.steps;

public class Steps extends StepsContainer {
    /**
     * Multi-ton instance
     **/
    private static ThreadLocal<Steps> currentThread = new ThreadLocal<>();

    /**
     * Return instance of class
     **/
    public static Steps getInstance() {
        if (currentThread.get() == null) {
            synchronized (Steps.class) {
                currentThread.set(new Steps());
            }
        }
        return currentThread.get();
    }

    /**
     * @param number mark steps to be taken from test rail
     */
    public static void fromTestRail(int number) {
        getInstance().stepInfo("TestRail Step " + number + " - ", "");
        getInstance().setStepAsTestRail(number);
    }

    /**
     * Get information from test rail preconditions
     */
    public static void fromTestRailPrecondition(){
        infoWithCaseProperty("Preconditions", "custom_preconds");
    }

    /**
     * Get information from test rail property
     */
    public static void infoWithCaseProperty(String message, String property){
        getInstance().stepInfo(message, "");
        getInstance().setWithTestRailCaseProperty(property);
    }

    /**
     * Log as "Step 1: .... "
     *
     * @param number
     * @param name
     */
    public static void info(int number, String name) {
        //MangoFinanceLogger.getMangoLogger().getLogger().info("Step " + String.valueOf(number) + ": " + name, "");
        getInstance().stepInfo("Step " + String.valueOf(number) + ": " + name, "");
    }

    /**
     * Log step with empty description
     *
     * @param name
     */
    public static void info(String name) {
        getInstance().stepInfo(name, "");
    }

    /**
     * Log step with description
     *
     * @param name
     * @param description
     */
    public static void info(String name, String description) {
        getInstance().stepInfo(name, description);
    }

    public static void description(String line) {
        getInstance().addToDescription(line);
    }

    /**
     * Private constructor
     */
    private Steps() {

    }

    /**
     * Sub steps
     * @param name
     * @param description
     */
    public static void subInfo(String name, String description) {
        getInstance().addToLastStep(name, description);
    }


    protected boolean isClassTest(String name) {
        return //Positive packages for tests
                ((name.startsWith("testcases.")
                        || name.startsWith("test.")) &&
                        //Negative packages for tests
                        (!name.startsWith("testcase.base")) &&
                        (!name.startsWith("testcases.base")) &&
                        (!name.startsWith("java.")) &&
                        (!name.startsWith("net.")) &&
                        (!name.startsWith("io.")) &&
                        (!name.startsWith("com.")) &&
                        (!name.startsWith("org.")));
    }
}
