package core.testng.steps;

import org.testng.ITestResult;
import core.testng.TestNgContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class StepsContainer {
    private Map<String, List<StepData>> steps = new HashMap<>();

    /**
     * @return top class caller in stack trace hierarchy
     */
    private String getCaller() {
        //get the id of thread
        return TestNgContext.getCurrentTestId();
    }

    /**
     * @return True if class found in test package
     */
    protected abstract boolean isClassTest(String name);

    protected void setStepAsTestRail(int num) {
        String key = getCaller();
        if (steps.containsKey(key)) {
            List<StepData> list = steps.get(key);
            list.get(list.size() - 1).setTestRailBased(true);
            list.get(list.size() - 1).setTestRailStep(num);
        }
    }

    protected void setWithTestRailCaseProperty(String property) {
        String key = getCaller();
        if (steps.containsKey(key)) {
            List<StepData> list = steps.get(key);
            list.get(list.size() - 1).setTestRailBased(true);
            list.get(list.size() - 1).setCaseProperty(property);
        }
    }

    protected void setWithTestRailSectionProperty(String property) {
        String key = getCaller();
        if (steps.containsKey(key)) {
            List<StepData> list = steps.get(key);
            list.get(list.size() - 1).setTestRailBased(true);
            list.get(list.size() - 1).setSectionProperty(property);
        }
    }

    /**
     * Add step with description
     *
     * @param name
     * @param description
     */
    public void stepInfo(String name, String description) {
        if (!(name.trim().endsWith(".")) &&
                !(name.trim().endsWith(":")))
            name += ".";

        String key = getCaller();
        StepData data = new StepData(name, description);
        if (steps.containsKey(key)) {
            List<StepData> list = steps.get(key);
            if (list.size() > 0)
                list.get(list.size() - 1).stop();
            list.add(data);
        } else {
            List<StepData> list = new ArrayList<>();
            list.add(data);
            steps.put(key, list);
        }
    }

    /**
     * Get steps for specified test
     *
     * @param iTestResult
     * @return
     */
    public List<StepData> get(ITestResult iTestResult) {
        List<StepData> result = new ArrayList<>();
        String name = iTestResult.getMethod().getId();
        if (steps.containsKey(name)) {
            result = steps.get(name);
        }
        return result;
    }

    public void clearSteps(ITestResult iTestResult) {
        String name = iTestResult.getMethod().getId();
        if (steps.containsKey(name)) {
            steps.put(name, new ArrayList<>());
        }
    }

    /**
     * Add text in the end of description
     *
     * @param text
     */
    public void addToDescription(String text) {
        List<StepData> result;
        String name = getCaller();

        //If no steps found add initial step
        if (!steps.containsKey(name)) {
            stepInfo("Initial step", "");
        }

        //Get last step
        result = steps.get(name);
        StepData data = result.get(result.size() - 1);
        //Add description
        if (data.getDescription().isEmpty()) {
            data.setDescription(text);
        } else
            data.setDescription(
                    data.getDescription() + "\n" + text);
    }

    public void addToLastStep(String title, String description){

        if (!steps.containsKey(getCaller()))
            stepInfo("Initial step", "");

        StepData data = getLastStep();
        data.addSubStep(new StepData(title, description));
    }

    /**
     * @return last step or null
     */
    public StepData getLastStep(){
        String name = getCaller();

        if (!steps.containsKey(name)){
            return null;
        }

        List<StepData> result = steps.get(name);
        return steps.get(name).get(result.size() - 1);
    }

}
