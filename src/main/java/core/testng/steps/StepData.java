package core.testng.steps;

import java.util.ArrayList;
import java.util.List;

public class StepData {
    private String name;
    private String description;
    private long start;
    private long stop = 0;
    private boolean isTestRailBased;
    private String sectionProperty = "";
    private String caseProperty = "";
    private int testRailStep;
    private List<StepData> subSteps = new ArrayList<>();
    public StepData() {
        this.start = System.currentTimeMillis();
    }

    public StepData(String name, String description) {
        this.name = name;
        this.description = description;
        this.start = System.currentTimeMillis();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getStop() {
        if (stop == 0)
            stop();
        return stop;
    }

    public void setStop(long stop) {
        this.stop = stop;
    }

    public void stop() {
        this.stop = System.currentTimeMillis();
    }

    public boolean isTestRailBased() {
        return isTestRailBased;
    }

    public void setTestRailBased(boolean testRailBased) {
        isTestRailBased = testRailBased;
    }

    public int getTestRailStep() {
        return testRailStep;
    }

    public void setTestRailStep(int testRailStep) {
        this.testRailStep = testRailStep;
    }

    public String getSectionProperty() {
        return sectionProperty;
    }

    public void setSectionProperty(String sectionPropertie) {
        this.sectionProperty = sectionPropertie;
    }

    public String getCaseProperty() {
        return caseProperty;
    }

    public void setCaseProperty(String casePropertie) {
        this.caseProperty = casePropertie;
    }

    public void addSubStep(StepData stepData){
        subSteps.add(stepData);
    }
}
