package core.testng;

import org.testng.ITestResult;
import org.testng.xml.XmlSuite;
import core.utility.testrail.TestCase;

import java.lang.reflect.Method;

public class Helper {

    @SuppressWarnings("unchecked")
    public static TestCase getTestCase(ITestResult iTestResult) {
        TestCase testCase = null;
        Method testMethod = iTestResult.getMethod().getMethod();
        if (testMethod.isAnnotationPresent(TestCase.class)) {
            testCase = testMethod.getAnnotation(TestCase.class);
        }
        return testCase;
    }

    public static String getMethodFullName(ITestResult iTestResult) {
        return iTestResult.getTestClass().getName() + "." + iTestResult.getMethod().getMethodName();
    }

    public static XmlSuite getCoreSuite(XmlSuite suite) {
        if (suite.getParentSuite() != null) {
            return getCoreSuite(suite.getParentSuite());
        } else {
            return suite;
        }
    }

    public static String getStatus(ITestResult iTestResult) {
        String result;
        switch (iTestResult.getStatus()) {
            case ITestResult.STARTED:
                result = "STARTED";
                break;
            case ITestResult.FAILURE:
                result = "FAILURE";
                break;
            case ITestResult.SKIP:
                result = "SKIP";
                break;
            case ITestResult.SUCCESS_PERCENTAGE_FAILURE:
                result = "SUCCESS_PERCENTAGE_FAILURE";
                break;
            case ITestResult.SUCCESS:
                result = "SUCCESS";
                break;
            default:
                result = "unknown";
                break;
        }
        return result;
    }
}
