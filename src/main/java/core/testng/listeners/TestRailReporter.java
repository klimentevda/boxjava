package core.testng.listeners;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import core.utility.testrail.TestCase;
import core.utility.testrail.TestRailUtils;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Properties;

public class TestRailReporter implements ITestListener {

    @Override
    public void onTestStart(ITestResult iTestResult) {
        sendResults(iTestResult);
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        sendResults(iTestResult);
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        sendResults(iTestResult);
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        sendResults(iTestResult);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
    
    private String getTestIdFromAnnotation(ITestResult result) {
        TestCase testData = null;
        Method testMethod = result.getMethod().getMethod();
        if (testMethod.isAnnotationPresent(TestCase.class)) {
            testData = testMethod.getAnnotation(TestCase.class);
        }
        return testData.id();
    }

    private void sendResults(ITestResult result) {
        TestRailUtils testRail = TestRailUtils.getInstance();
        Properties sysProps = System.getProperties();
        String testRunId = sysProps.getProperty("runId");
        if ((sysProps.getProperty("runId") != null) && (!sysProps.getProperty("runId").equals(""))) {
            try {
                switch (result.getStatus()) {
                    case ITestResult.STARTED:
                        testRail.addResultForCase(testRunId, getTestIdFromAnnotation(result), 6, "Test in progress");
                        break;
                    case ITestResult.SUCCESS:
                        testRail.addResultForCase(testRunId, getTestIdFromAnnotation(result), 1, "This automated test is Passed!");
                        break;
                    case ITestResult.FAILURE:
                        testRail.addResultForCase(testRunId, getTestIdFromAnnotation(result), 5, result.getThrowable().getMessage());
                        break;
                    case ITestResult.SKIP:
                        testRail.addResultForCase(testRunId, getTestIdFromAnnotation(result), 4, "Test Skipped, Retest");
                        break;
                }
            } catch (Exception ex) {
                System.out.println(Arrays.toString(ex.getStackTrace()));
            }
        }
    }
}
