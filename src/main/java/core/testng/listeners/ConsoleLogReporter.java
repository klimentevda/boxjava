package core.testng.listeners;

import core.logger.MangoFinanceLogger;
import org.apache.commons.lang3.StringUtils;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

public class ConsoleLogReporter implements ITestListener {

    @Override
    public void onTestStart(ITestResult iTestResult) {
        String testStarted = "Test: \"" +
                iTestResult.getTestClass().getName() + "." +
                iTestResult.getMethod().getMethodName()
                + "\" started.";
        println(StringUtils.repeat("_", testStarted.length()));
        println(testStarted);
        println(StringUtils.repeat("_", testStarted.length()));
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        printTestResults(iTestResult);
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        printTestResults(iTestResult);
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        printTestResults(iTestResult);
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }

    private void printTestResults(ITestResult result) {
        String testCompleted =
                "Test: \"" +
                result.getTestClass().getName() +
                "." +
                result.getMethod().getMethodName()
                + "\" completed.";

        println(StringUtils.repeat("_", testCompleted.length()));
        println(testCompleted);
        println("Test Method resides in " + result.getTestClass().getName());

        if (result.getParameters().length != 0) {
            String params = null;

            for (Object parameter : result.getParameters()) {
                params += parameter.toString() + ",";
            }

            println("Test Method had the following parameters : " + params);

        }

        String status = null;

        switch (result.getStatus()) {

            case ITestResult.SUCCESS:

                status = "Pass";

                break;

            case ITestResult.FAILURE:

                status = "Failed";

                break;

            case ITestResult.SKIP:

                status = "Skipped";
        }

        println("Test Status: " + status);

        if (result.getStatus() == ITestResult.FAILURE) {
            println(StringUtils.repeat("*", testCompleted.length()));
            println("Cause: \n" + result.getThrowable());
        }
        println(StringUtils.repeat("_", testCompleted.length()));
    }

    private void println(String info){
        String prefix = new SimpleDateFormat().format(new Date());
        System.out.println("[" + prefix + "]["+Thread.currentThread().getName() + "] " + info);
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.FINE, info);
    }
}
