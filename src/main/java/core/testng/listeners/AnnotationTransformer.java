package core.testng.listeners;

import org.testng.*;
import org.testng.annotations.ITestAnnotation;
import core.utility.TestCaseManager;
import core.utility.testrail.TestCase;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class AnnotationTransformer  implements IAnnotationTransformer, IInvokedMethodListener {

    private String getCaseId(Method method, Class testClass) {
        //In case annotation will be placed on test method
        if (method!= null) {
            if (method.isAnnotationPresent(TestCase.class)) {
                return method.getAnnotation(TestCase.class).id();
            }
        }
        //In case annotation will be placed on test class
        if (testClass != null){
            if (testClass.isAnnotationPresent(TestCase.class)) {
                return ((TestCase)testClass.getAnnotation(TestCase.class)).id();
            }
        }
        return null;
    }

    @Override
    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        annotation.setRetryAnalyzer(RetryAnalyzer.class);
        if (TestCaseManager.casesToRun != null) {
            annotation.setEnabled(
                    TestCaseManager.casesToRun.stream().anyMatch(str -> str.equals(getCaseId(testMethod, testClass))));
        }
    }

    @Override
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {

    }

    @Override
    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        IRetryAnalyzer retry = iTestResult.getMethod().getRetryAnalyzer();
        if (retry == null) {
            return;
        }
        iTestResult.getTestContext().getFailedTests().removeResult(iTestResult.getMethod());
        iTestResult.getTestContext().getSkippedTests().removeResult(iTestResult.getMethod());
    }
}