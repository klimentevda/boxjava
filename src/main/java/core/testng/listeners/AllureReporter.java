package core.testng.listeners;

import io.qameta.allure.Allure;
import io.qameta.allure.model.*;
import core.logger.MangoFinanceLogger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;
import core.selenium.driver.DriverManager;
import core.testng.Helper;
import core.testng.TestNgContext;
import core.testng.steps.StepData;
import core.testng.steps.Steps;
import core.utility.testrail.TestCase;
import core.utility.testrail.TestCaseData;

import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public class AllureReporter implements IInvokedMethodListener {
    private final String squareStile = "style=\"background-color:#f8f9fa; border-radius: 4px; border: 1px solid #ebedef;\"";

    private void addDescription(ITestResult iTestResult) {
        TestCaseData caseData = new TestCaseData();
        boolean found = false;
        //Load case data from annotation
        TestCase testCase = Helper.getTestCase(iTestResult);
        if (testCase != null) {
            caseData.load(testCase);
            found = true;
        }
        //Load case data from function name
        else {
            if (iTestResult.getMethod().getMethodName().contains("_")) {
                String caseID = iTestResult.getMethod().getMethodName().split("_")[0].substring(1);
                MangoFinanceLogger.getMangoLogger().getLogger().log(Level.FINE, "Case id: " + caseID);
                caseData.load(caseID);
                found = true;
            }
        }
        //If case have data fill allure
        if (found) {

            //Add test status
            String status;
            String value = caseData.getStatus();
            if (caseData.getCustomStatus() != 2) {
                status = "Test case status: <font color=\"red\">\"" + value + "\"</font>";
            } else
                status = "Test case status: <font color=\"green\">\"" + value + "\"</font>";

            //Add test case url
            String testcaseUrl =
                    "<a href = \"" + caseData.getURL() + "\" " + squareStile + ">" +
                            "Test Case ID: " + caseData.getId() + "</a>";

            //Add test case description
            String customDescr =
                    "<pre " + squareStile + "><h3>Case description:</h3><br/>" + caseData.getDescription() + "</pre>";

            //String description = "<h3  " + squareStile + "> " +title + ":</h3> </br>" +
            String description =
                    status + "</br>" +
                            testcaseUrl + "</br>" +
                            customDescr + "</br>";
            if (isCasePresent()) {
                String currentCase = Allure.getLifecycle().getCurrentTestCase().get();
                Allure.getLifecycle().updateTestCase(currentCase,
                        s -> s
                                .setName(caseData.getTitle())
                                .setLabels(new Label().setName("severity").setValue(caseData.getSeverity()))
                                .setLabels(new Label().setName("epic").setValue(caseData.getStatus()),
                                        new Label().setValue("story").setValue(caseData.getFeature()))
                                .setDescription(description));
            }
        }
    }

    private byte[] takeScreenShot() {
        return ((TakesScreenshot) DriverManager.currentDriver()).getScreenshotAs(OutputType.BYTES);
    }

    private void addSteps(ITestResult iTestResult) {
        List<StepData> steps = Steps.getInstance().get(iTestResult);

        for (int i = 0; i < steps.size(); i++) {
            StatusDetails details = new StatusDetails();
            StepData data = steps.get(i);

            String stepDescription = null;
            String stepExpectation = null;

            //If case should be taken from test rail
            if (data.isTestRailBased()) {
                //Update test case with test rail information
                TestCaseData caseData = new TestCaseData();
                //Load case data from annotation
                TestCase testCase = Helper.getTestCase(iTestResult);
                if (testCase != null) {
                    caseData.load(testCase);
                }
                //Load case data from function name
                else {
                    if (iTestResult.getMethod().getMethodName().contains("_"))
                        caseData.load(iTestResult.getMethod().getMethodName().split("_")[0]);
                }
                //If case have data fill allure
                if (caseData != null) {
                    String description = "No data";
                    if (caseData.getSteps() != null)
                        //Check if it information - case property
                        if (!data.getCaseProperty().isEmpty()){
                            description = (String)caseData.getCaseProperty(data.getCaseProperty());
                        }
                        //Check if it information about test step
                        if ((caseData.getSteps().size() > 0)
                                && data.getTestRailStep() > 0) {
                            description =
                                    "------------------------------------------------------------\n"
                                            + "Step actions:   --------------------------------------------\n"
                                            + "------------------------------------------------------------\n"
                                            + "\n"
                                            + caseData.getSteps().get(data.getTestRailStep() - 1).getKey()
                                            + "\n"
                                            + "\n------------------------------------------------------------\n"
                                            + "Expected:       --------------------------------------------\n"
                                            + "------------------------------------------------------------\n"
                                            + "\n"
                                            + caseData.getSteps().get(data.getTestRailStep() - 1).getValue();
                            stepDescription = caseData.getSteps().get(data.getTestRailStep() - 1).getKey();
                            stepExpectation = caseData.getSteps().get(data.getTestRailStep() - 1).getValue();
                            data.setName(data.getName() + String.format("'%.23s...'", stepDescription));
                        }
                    data.setDescription(description);
                }
            }

            if (!data.getDescription().isEmpty())
                details.setMessage(data.getDescription());

            Status status = Status.PASSED;
            if (i == steps.size() - 1) {
                if (!iTestResult.isSuccess()) {
                    status = Status.FAILED;
                }
            }

            //Add step to report
            if (isCasePresent()) {

                StepResult result = new StepResult()
                        .setName(data.getName())
                        .setStart(data.getStart())
                        .setStop(data.getStop())
                        .setStatusDetails(details)
                        .setStatus(status);

                String uuid = UUID.randomUUID().toString();
                Allure.getLifecycle().startStep(uuid, result);

                if (!data.getDescription().isEmpty()){
                    //add step description
                    if (stepDescription != null){
                        Allure.getLifecycle().addAttachment("step description:", "text/html", "", stepDescription.getBytes());
                    }else{
                        Allure.getLifecycle().addAttachment("details:", "text/html", "", data.getDescription().getBytes());
                    }
                    //add step expectations
                    if (stepExpectation != null){
                        if (!stepExpectation.isEmpty()) {
                            Allure.getLifecycle().addAttachment("step expectations:", "text/html", "", stepExpectation.getBytes());
                        }
                    }
                }

                Allure.getLifecycle().stopStep(uuid);

                Allure.getLifecycle().updateStep(uuid, stepResult -> stepResult
                        .setStatusDetails(details)
                        .setStart(data.getStart())
                        .setStop(data.getStop()));
            }
        }

        // In case of no test steps and test failure attach screen to root.
        if ((!iTestResult.isSuccess())
                && (DriverManager.isRunned())) {
            Allure.getLifecycle().addAttachment("Failure screenshot", "image/png", "", takeScreenShot());
        }

    }

    private String createStep(StepResult result){
        String uuid = UUID.randomUUID().toString();
        Allure.getLifecycle().startStep(uuid, result);
        Allure.getLifecycle().stopStep(uuid);
        return uuid;
    }

    private boolean isCasePresent() {
        return Allure.getLifecycle().getCurrentTestCase().isPresent();
    }

    @Override
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        TestNgContext.setCurrentTestId(iInvokedMethod.getTestMethod().getId());
        if (iInvokedMethod.isTestMethod()) {
            addDescription(iTestResult);
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        if (iInvokedMethod.isTestMethod()) {
            addSteps(iTestResult);
            Steps.getInstance().clearSteps(iTestResult);
        }
    }

}
