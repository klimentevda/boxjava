package core.testng.listeners;

import core.logger.MangoFinanceLogger;
import org.apache.commons.lang3.StringUtils;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import core.utility.PropertiesLoader;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

public class RetryAnalyzer implements IRetryAnalyzer {

    public final static String RERTY_COUNT = "testNg.retry.count";

    int counter = 0;
    int retryLimit = Integer.valueOf(PropertiesLoader.load().getProperty(RERTY_COUNT, "3"));

    @Override
    public boolean retry(ITestResult result) {

        if (result.getThrowable() instanceof AssertionError)
            return false;

        if(counter < retryLimit)
        {
            counter++;

            String message = "Retry test " + counter + " time, method "
                    + result.getTestClass().getName() +
                    "." +
                    result.getMethod().getMethodName();
            println(StringUtils.repeat("+", message.length()));
            println(message);
            println(StringUtils.repeat("+", message.length()));
            return true;
        }
        return false;
    }

    private void println(String info){
        String prefix = new SimpleDateFormat().format(new Date());
        System.out.println("[" + prefix + "]["+Thread.currentThread().getName() + "] " + info);
        MangoFinanceLogger.getMangoLogger().getLogger().log(Level.FINE, info);
    }

}
