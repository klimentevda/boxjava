package logic;

import core.selenium.driver.DriverManager;
import core.utility.PropertiesLoader;


/**
 * Classed used to navigation between pages
 */
public class Navigate {

    /**
     * To main page
     */
    public static void toMain(){
        DriverManager.currentDriver().get(getCommonSystemURL());
    }

    /**
     * To rainbow upload page
     */
    public static void toRainbow(){
        DriverManager.currentDriver().get(getRainbowURL());
    }

    /**
     * To application create page
     */
    public static void toApplicationCreate(){
        toRelative("applications/add");
    }

    /**
     * To PaymentBuilder create page
     */
    public static void toPaymentCreate(String AddPayment){
        toRelative(AddPayment);
    }


    /**
     * To Transactions create page
     */
    public static void toTransactions(String AddPayment){
        toRelative(AddPayment);
    }


    /**
     * To any page relative to main
     * @param path
     */
    public static void toRelative(String path){
        String commonURL = getCommonSystemURL();
        if (!commonURL.endsWith("/"))
            commonURL += "/";
        DriverManager.currentDriver().get(commonURL + path);
    }

    public static void login() {
        String erpMangoUsername = PropertiesLoader.getPropertie("common.system.username").orElse("");
        String erpMangoPassword = PropertiesLoader.getPropertie("common.system.password").orElse("");

    }

    public static String getCommonSystemURL(){
        return PropertiesLoader.getPropertie("common.system.url").get();
    }

    private static String getRainbowURL(){
        return PropertiesLoader.getPropertie("rainbow.url").get();
    }
}
