package mail;

import core.selenium.driver.DriverManager;
import core.selenium.driver.WebDriverSettings;
import logic.Navigate;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.Pages;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    @BeforeMethod
    public void openBrowser(){
        //Open browser
        Navigate.toMain();
        //implicit waits
        DriverManager.currentDriver()
                .manage()
                .timeouts()
                .implicitlyWait(WebDriverSettings.getInstance().getElementWait(), TimeUnit.SECONDS);
        DriverManager
                .currentDriver()
                .manage()
                .timeouts()
                .pageLoadTimeout(WebDriverSettings.getInstance().getElementWait(), TimeUnit.SECONDS);
        //initialization of pages
        Pages.initialize();
        //login
        Navigate.login();
        //chose branch
    }

    @AfterMethod(alwaysRun = true)
    public void closeBrowser(){
        //Close browser, destroy selenium session
        DriverManager.getInstance().destroy();
    }
}
